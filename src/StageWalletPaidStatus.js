import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-accessible-accordion/dist/fancy-example.css';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Slide from '@material-ui/core/Slide';
import SweetAlert from 'react-bootstrap-sweetalert';

import ExportToExcel from './ExportToExcel';

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
class StageWalletPaidStatus extends Component {

    constructor(props) {
        super(props)
        this.state = {
            availableBalance: '',
            isDialogOpen: false,
            schemeId:'',
            amount:'',
            alert:null,
			startDate:'',
			toDate:'',
			stage2CustomerWallet: [],
			stage3CustomerWallet: [],
			stage4CustomerWallet: [],
			stage5CustomerWallet: [],
			stage6CustomerWallet: [],
			stage7CustomerWallet: [],
			stage8CustomerWallet: [],
			stage9CustomerWallet: [],
			stage1CustomerWallet:[]
        
		}
    }
    handlePay = (e) => {
		
		
		
        const getAlert = () => (
            <SweetAlert
              success
              title={"Payment updated successfully!"}
              onConfirm={() => this.hideAlert()}
              value={"Payment Updated"}
            />
            
          );
       this.setState(
	   {alert:getAlert(),
    isDialogOpen:false});
      }
	  
	  
	  
	  
	 dateFilterLevel1 = (e) => {
		  console.log( 'dateFilterSubmit1');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }
	 
 dateFilterLevel2 = (e) => {
		  console.log( 'dateFilterSubmit2');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level2'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage2CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel3 = (e) => {
		  console.log( 'dateFilterSubmit3');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level3'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage3CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel4 = (e) => {
		  console.log( 'dateFilterSubmit4');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level4'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage4CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel5 = (e) => {
		  console.log( 'dateFilterSubmit5');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level5'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage5CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel6 = (e) => {
		  console.log( 'dateFilterSubmit6');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level6'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage6CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel7 = (e) => {
		  console.log( 'dateFilterSubmit7');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level7'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage7CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel8 = (e) => {
		  console.log( 'dateFilterSubmit8');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level8'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage8CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel9 = (e) => {
		  console.log( 'dateFilterSubmit9');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level9'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage9CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }	  
	  
	  
	  stage1Click = (e) => {
		console.log( "Stage 1 clicked");
		
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }
	  stage2Click = (e) => {
		console.log( "Stage 2 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level2'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage2CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage3Click = (e) => {
		console.log( "Stage 3 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level3'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage3CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage4Click = (e) => {
		console.log( "Stage 4 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level4'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage4CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage5Click = (e) => {
		console.log( "Stage 5 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level5'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage5CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage6Click = (e) => {
		console.log( "Stage 6 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level6'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage6CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage7Click = (e) => {
		console.log( "Stage 7 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level7'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage7CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage8Click = (e) => {
		console.log( "Stage 8 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level8'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage8CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage9Click = (e) => {
		console.log( "Stage 9 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level9'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage9CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }	  
	  
    hideAlert=() =>{
        this.setState({alert:null});
    }
    handleClose = () => {
        this.setState({
            isDialogOpen:false
          })
      };
      handleOpen = (e) => {
          console.log(e.target.value);
          var i = this.state.stage1CustomerWallet.find(item=>{
              return item.schemeId===e.target.value
          })
        if(i){
        this.setState({
            isDialogOpen:true,
            schemeId:i.schemeId,
            amount:i.stageAmount         
          })
        }
      };
        
    componentDidMount() {
        var schemeID = localStorage.getItem('schemeId');
        const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: '',
          toDate: '',
		   stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
		
         
			
			
    }
   
    rowWalletPay(original){
        //return(<span></span>);
        return(
		(original.stageWalletStatus) ?(<span>{original.stageWalletStatus}</span>):(<button padding="5px" borderRadius="5px" value={original.schemeId} onClick={this.handleOpen} >Pay</button>));
    };


    render() {
        const columns = [
            {
                Header: "Scheme ID",
                accessor: "schemeId"
            },
			{
                Header: "First Name",
                accessor: "schemeFirstName"
            },
            {
                Header: "Completed Date",
                accessor: "stageCompletedDate"
            },
			{
				 Header: "Paid Date",
                accessor: "stagePaidDate"
				
			},			
            {
                Header: "Amount",
                accessor: "stageAmount",
				sortable: false,
				filterable: false
            }
            ,
			 {
                Header: "Wallet Status",
                accessor: "stageWalletStatus",
				sortable: false,
				filterable: false,
                Cell:({row, original})  =>  this.rowWalletPay( original),//{props.value==="paid"?(<span value={props.value}>{props.value}</span>):(<button value={props.value}>Pay</button>)},
            }
        ]
	
	  const {alert}=this.state;
        return (
            <div id="wallet">
                <h1>Scheme Wallet Paid History</h1>
				
				
				<Accordion allowZeroExpanded preExpanded={['expire']}>
            <AccordionItem uuid="expire">
                <AccordionItemHeading onClick={this.stage1Click}>
                    <AccordionItemButton>
                       STAGE 1
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
				
		    <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel1} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage1CustomerWallet}
				defaultPageSize={10}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
			

            <AccordionItem>
                <AccordionItemHeading onClick={this.stage2Click}>
                    <AccordionItemButton  >
                        STAGE 2
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   
				     <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel2} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage2CustomerWallet}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				   
				   
				 
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage3Click}>
                    <AccordionItemButton>
                        STAGE 3
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  
				      <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel3} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage3CustomerWallet}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				  
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage4Click}>
                    <AccordionItemButton>
                        STAGE 4
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  
				      <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel4} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage4CustomerWallet}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >

{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage5Click}>
                    <AccordionItemButton>
                        STAGE 5
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   
				       <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel5} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage5CustomerWallet}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage6Click}>
                    <AccordionItemButton>
                        STAGE 6
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   
				       <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel6} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage6CustomerWallet}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				
                </AccordionItemPanel>
            </AccordionItem>
				
            
        </Accordion>
        <Dialog
        open={this.state.isDialogOpen}
        //TransitionComponent={Transition}
        keepMounted
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{"Wallet Pay"}</DialogTitle>
        <DialogContent>
        <div align="left" style={{marginLeft:"",display: 'flex', flexWrap:"wrap"}}>       
        <TextField
          id="standard-number"
          label="Scheme ID"
          value={this.state.schemeId || ''}  
          InputProps={{readOnly: true,}}  
          style={{width: '25ch', margin:"1em"}}                
        /> 
         <TextField
          id="standard-number"
          label="Amount"
          value={this.state.amount || ''}  
          InputProps={{readOnly: true,}}  
          style={{width: '25ch', margin:"1em"}}                   
        /> 
        </div >
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handlePay} color="primary">
            Pay
          </Button>
          <Button onClick={this.handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      {this.state.alert}
            </div>

        )

    }
}

export default StageWalletPaidStatus;


