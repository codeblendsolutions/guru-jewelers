import React, { PureComponent } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import Popup from "reactjs-popup";
import Tree from 'react-d3-tree';
import "./App.css";
import DatePicker from "react-datepicker";
import * as myConstClass from './Constants.js';
import "react-datepicker/dist/react-datepicker.css";
import LoadingOverlay from 'react-loading-overlay';
import Loader from 'react-loader-spinner';

//import matchSorter from 'match-sorter'

class MemberSearch extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
		 overlayVisible:false,
		 showLoader: false,
      schemeDetails: [],
      alert: null,
      userDetail: {
        schemeUserDetails: "",
        userAddressDetails: "",
        userBankDetails: ""
      },
      refferalData: [
        {
          "name": "SGJ43456",
          "parent": "null"
        },
        {
          "name": "SGJ43456",
          "parent": "null"
        }
      ],
      showPopup: true,
      open: false,
      openTree: false,
      filtered: [],
      startDate: new Date(),
      toDate: new Date()
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModalTree = this.openModalTree.bind(this);
    this.closeModalTree = this.closeModalTree.bind(this);

  }

  handleStartDateChange = date => {
    this.setState({
      startDate: date
    });
  };
  handleToDateChange = date => {
    this.setState({
      toDate: date
    });
  };
  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    document.body.style.overflow = 'auto'
    this.setState({ open: false });
  }

  openModalTree() {
    this.setState({ openTree: true });
  }
  closeModalTree() {
    document.body.style.overflow = 'auto'
    this.setState({ openTree: false });
  }


  componentDidMount() {
    var schemeID = 'SGJNS00603';
    var url = myConstClass.MEMBER_DETAILS + schemeID;
    console.log(url);
	
	 this.setState({
            showLoader: false,
            overlayVisible:true        })

    Axios.get(url)
      .then((response) => {
		  
	

        this.setState({
          //  availableBalance: response.data.availableBalance,
		  overlayVisible:false,
                    showLoader: false,
          schemeDetails: response.data

        })
        console.log("schemeDetails : " + this.state.schemeDetails[1].schemeId);

      })

      .catch(error => {
        console.log(error);
      });
  }


  handleOnClick = async (e) => {
    e.preventDefault();
    //var userDetail='';
    var url = myConstClass.SCHEME_FORM_URL + '/' + e.target.innerText;
    console.log("url : " + url);
	
	
			
    await Axios.get(url)
      .then((response) => {
        this.setState({

          userDetail: response.data,
          open: true
        })
      })
      .catch(error => {
        console.log(error);
      });
    document.body.style.overflow = 'hidden'
  }
  handleTree = async (e) => {
    var url = myConstClass.REFERRAL_TREE_VIEW + e.target.value;
    console.log("url : " + url);
    await Axios.get(url)
      .then((response) => {
        this.setState({
          refferalData: response.data,
          openTree: true
        })
      })
      .catch(error => {
        console.log(error);
      });

    document.body.style.overflow = 'hidden'
    var data = this.state.refferalData;
    console.log("orgData : " + data);

    var dataMap = data.reduce(function (map, node) {
      map[node.name] = node;
      return map;
    }, {});

    var myTreeData = [];
    data.forEach(function (node) {
      // add to parent
      var parent = dataMap[node.parent];
      if (parent) {
        // create child array if it doesn't exist
        (parent.children || (parent.children = []))
          // add node to child array
          .push(node);
      } else {
        // parent is null or missing
        myTreeData.push(node);
      }
    });
    const nodeSvgShape = { shape: 'image', shapeProps: { href: "/images/coin.png", width: 40, height: 40, x: -20, y: - 35, } };
    const linkSvgShape = { stroke: 'green', strokeWidth: 3 };

  }
  dateFilterSubmit = async (e) => {
    var schemeID = 'SGJNS00603';
    var fromD = this.state.startDate.getFullYear() + "-" + parseInt(this.state.startDate.getMonth() + 1) + "-" + this.state.startDate.getDate();
    var toD = this.state.toDate.getFullYear() + "-" + parseInt(this.state.toDate.getMonth() + 1) + "-" + this.state.toDate.getDate();
    var url = myConstClass.MEMBER_DETAILS + schemeID + '/' + fromD + "/" + toD;
    console.log(url);
	
	this.setState({
            showLoader: false,
            overlayVisible:true        })
			
    Axios.get(url)
      .then((response) => {

        this.setState({
          //  availableBalance: response.data.availableBalance,
		  overlayVisible:false,
                    showLoader: false,
          schemeDetails: response.data

        })
        //console.log("schemeDetails : " + this.state.schemeDetails[1].schemeId);

      })

      .catch(error => {
        console.log(error);
      });

  }

  render() {
    const columns = [
      {
        Header: "ID",
        id: "schemeId",
        accessor: "schemeId",
		  accessor: d => `${d.schemeUserDetails.schemeId}`,
        headerStyle: { textAlign: 'center', backgroundColor: 'powderblue', fontWeight: "bold" },
        //filterMethod: (filter, row) => row[filter.id].startsWith(filter.value),            
     
//	 Cell: e => <span><a href={e.value} onClick={this.handleOnClick} value={e.value}> {e.value} </a> <button value={e.value} onClick={this.handleTree}>View tree</button></span>

      },
      {
        Header: "Name",
        id: "Name",
        accessor: d => `${d.schemeUserDetails.firstName}`,//"schemeUserDetails.firstName",            
        headerStyle: { textAlign: 'center', backgroundColor: 'powderblue', fontWeight: "bold" },
        //filterMethod: (filter, row) => row[filter.id].startsWith(filter.value),
        Cell: row => <div style={{ textAlign: "left" }}>{row.value}</div>

      },
      {
        Header: "Refferal id",
        accessor: "referrerUserId",
        id: "referrerUserId",
		accessor: d => `${d.schemePackageDetails.referrerUserId}`,
        headerStyle: { textAlign: 'center', backgroundColor: 'powderblue', fontWeight: "bold" },

      },
      {
        Header: "Joined on",
        accessor: "schemeCreatedDate",
        id: "schemeCreatedDate",
		accessor: d => `${d.schemePackageDetails.schemeCreatedDate}`,
        headerStyle: { textAlign: 'center', backgroundColor: 'powderblue', fontWeight: "bold" },

      }
      ,
      {
        Header: "Wallet Balance",
        accessor:  d => `${d.schemePackageDetails.availableWalletBalance}`,
        id: "walletBalance",
        headerStyle: { textAlign: 'center', backgroundColor: 'powderblue', fontWeight: "bold" },

      }
    ]
    const Modal = () => (

      <Popup
        style={{ width: 'auto', overflow: 'auto' }}
        min-size="80%"
        open={this.state.open}
        modal
        closeOnDocumentClick
        onClose={this.closeModal}
      >
        <div className="schemeContent">
          <div>
            <label>FIRST NAME<span style={{ color: "red", alignSelf: "center" }}>*</span></label></div>
          <div>
            <input styple={{ alignSelf: "center" }} type="text" name="firstName" placeholder="First Name" defaultValue={this.state.userDetail.schemeUserDetails.firstName} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>LAST NAME</label></div>
          <div> <input type="text" name="lastName" placeholder="Last Name" defaultValue={this.state.userDetail.schemeUserDetails.lastName} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>MOBILE NUMBER<span style={{ color: "red" }}>*</span></label></div>
          <div> <input type="text" name="phoneNumber" placeholder="Phone Number" defaultValue={this.state.userDetail.schemeUserDetails.mobile} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>GENDER<span style={{ color: "red" }}>*</span></label></div>
          <div>
            <input type="radio" name="gender" onChange={this.onRadioChange} defaultChecked={this.state.userDetail.schemeUserDetails.gender === 'male'} value="male" /> Male
            <input type="radio" name="gender" onChange={this.onRadioChange} defaultChecked={this.state.userDetail.schemeUserDetails.gender === 'female'} value="female" /> Female

            </div>
          <div>
            <label>DATE OF BIRTH</label></div>
          <div> <input type="text" name="dob" placeholder="Date of Birth" defaultValue={this.state.userDetail.schemeUserDetails.dateOfBirth} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>ADDRESS 1<span style={{ color: "red" }}>*</span></label></div>
          <div> <input type="text" name="address1" placeholder="Address1" defaultValue={this.state.userDetail.userAddressDetails.address1} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>ADDRESS 2</label></div>
          <div> <input type="text" name="address2" placeholder="Address2" defaultValue={this.state.userDetail.userAddressDetails.address2} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>CITY</label></div>
          <div> <input type="text" name="city" placeholder="City" defaultValue={this.state.userDetail.userAddressDetails.city} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>PINCODE</label></div>
          <div> <input type="text" name="pincode" placeholder="Pincode" defaultValue={this.state.userDetail.userAddressDetails.city} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>EMAIL ID</label></div>
          <div>  <input type="text" name="email" defaultValue={this.state.userDetail.schemeUserDetails.email} placeholder="Email" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>PAN CARD</label></div>
          <div> <input type="text" name="pancard" defaultValue={this.state.userDetail.userBankDetails.pan} placeholder="Pan Card" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>BANK NAME</label></div>
          <div> <input type="text" name="bankName" defaultValue={this.state.userDetail.userBankDetails.bankName} placeholder="Bank Name" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>BRANCH</label></div>
          <div> <input type="text" name="branch" defaultValue={this.state.userDetail.userBankDetails.branch} placeholder="Bank branch" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>ACCOUNT NUMBER</label></div>
          <div> <input type="text" name="accountNo" defaultValue={this.state.userDetail.userBankDetails.accountNumber} placeholder="Bank Account Number" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>IFSC CODE</label></div>
          <div> <input type="text" name="ifscCode" defaultValue={this.state.userDetail.userBankDetails.ifscCode} placeholder="Bank IFSC Code" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>
            <label>SPONSER ID</label></div>
          <div> <input type="text" name="spnserId" disabled={true} defaultValue={this.state.userDetail.referrerUserId} placeholder="Bank IFSC Code" onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>

        </div>
      </Popup>
    );
    var data = this.state.refferalData;
    console.log("orgData : " + data);

    var dataMap = data.reduce(function (map, node) {
      map[node.name] = node;
      return map;
    }, {});

    var myTreeData = [];
    data.forEach(function (node) {
      // add to parent
      var parent = dataMap[node.parent];
      if (parent) {
        // create child array if it doesn't exist
        (parent.children || (parent.children = []))
          // add node to child array
          .push(node);
      } else {
        // parent is null or missing
        myTreeData.push(node);
      }
    });
    const nodeSvgShape = { shape: 'image', shapeProps: { href: "/images/coin.png", width: 40, height: 40, x: -20, y: - 35, } };
    const linkSvgShape = { stroke: 'green', strokeWidth: 3 };

    const ModalTree = () => (

      <Popup
        className="popupDialog"
        style={{ width: '100%', overflow: 'auto' }}
        width="80%"
        open={this.state.openTree}
        modal
        closeOnDocumentClick
        onClose={this.closeModalTree}
      >
        <div id="treeWrapper" style={{ width: '100%', height: '40em', alignSelf: "center", justifyContent: "center" }}>

          <Tree style={{ alignSelf: "center", justifyContent: "center" }} data={myTreeData} nodeSvgShape={nodeSvgShape} orientation={"vertical"} translate={{ x: 700, y: 50 }}
            allowForeignObjects
            nodeLabelComponent={{
              render: <NodeLabel className='myLabelComponentInSvg' />,
              foreignObjectWrapper: {
                x: -5,
                y: -24
              }
            }}
            styles={{ alignself: "center" }}
            zoom={0.65}
            pathFunc="elbow" styles={{ links: linkSvgShape }} textLayout={{ textAnchor: "start", x: 18, y: -20 }} translate={{ x: 580, y: 35 }} />
        </div>
      </Popup>
    );
    const DateInput = ({ value, onClick }) => (
      <button style={{ fontSize: "20px", fontWeight: "500" }} onClick={onClick}>
        {value}
      </button>
    );

    return (

      <div id="wallet">
	  
	  <LoadingOverlay
  active={this.state.overlayVisible}
  spinner
  text='Logging Data...'
  >
        <h1 >Member  Details</h1>
        <Modal />
        <ModalTree style={{ width: "80%" }} />
        <div style={{ backgroundColor: 'powderblue' }}>
          <span style={{ fontSize: "23px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={this.handleStartDateChange}
            maxDate={new Date()}
            customInput={<DateInput />}
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "23px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={this.handleToDateChange}
            minDate={this.state.startDate}
            maxDate={new Date()}
            customInput={<DateInput />}
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "20px", fontWeight: "500", borderRadius: "10px", padding: "3px", backgroundColor: "THISTLE", }} onClick={this.dateFilterSubmit} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
        <ReactTable
          style={{
            //width: '100%',
            backgroundColor: 'HONEYDEW',
            display: 'flex',
            justifyContent: 'center',
            align: 'middle'
          }}
          filterable

          defaultFilterMethod={(filter, row) =>
            (String(row[filter.id])).toLowerCase().startsWith((filter.value).toLowerCase())}


          columns={columns}
          data={this.state.schemeDetails}
          className="-striped -highlight"
          defaultPageSize={25}
        >

        </ReactTable>

        {this.state.alert}
		
		 <div className='loader-spinner' >
                    <Loader type="Plane" color="#00BFFF" visible={this.state.showLoader} />
                </div>
                </LoadingOverlay>

      </div>

    )

  }
}
class NodeLabel extends React.PureComponent {
  render() {
    const { className, nodeData } = this.props
    return (
      <div className={className}>
        <span><b>{nodeData.name}</b></span>
        <span><br /><b>{nodeData.value}</b></span>

      </div>
    )
  }
}
export default MemberSearch;


