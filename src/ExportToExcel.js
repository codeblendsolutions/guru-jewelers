import React, {Component} from 'react';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class ExportToExcel extends Component {

    constructor(props) {
        super(props);
    }
 
    render() {
 
        return (
            <div>
                <ReactHTMLTableToExcel
                    id="test-table-xls-button"
                    className="export"
                    table="table-to-xls"
                    filename="StageDetails"
                    sheet="SheetDetail"
                    buttonText="Export"/>
                <table hidden={true} id="table-to-xls">
				<thead>
                    <tr>
                        <th>Scheme ID</th>
						<th>First Name</th>
                        <th>Completed Date</th>
                        <th>Paid Date</th>
						<th>Amount</th>
                    </tr>
					</thead>
					<tbody>
					{
						this.props.posts.map(post => {
							return(
							<tr key={post.schemeId} >
							<td>{post.schemeId}</td>
							<td>{post.schemeFirstName}</td>
							<td>{post.stageCompletedDate}</td>
							<td>{post.stagePaidDate}</td>
							<td>{post.stageAmount}</td>
							</tr>
							)
						})
					}
					
					
					</tbody>
                </table>
 
            </div>
        );
    }

}

export default ExportToExcel;


