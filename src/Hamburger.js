import React, { Component } from 'react';
import './Hamburger.css';
import ReferralTree from './ReferralTree';
import SideBar from './SideBar';
import SchemeForm from './SchemeForm';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MemberSearch from './MemberSearch';
import Wallet from './Wallet';
import WalletPaidHistory from './WalletPaidHistory';
import WalletTopupRequestForm from './WalletTopupRequestForm';
import WalletPay from './WalletPay';
import WalletRequest from './WalletRequest';
import ChangePassword  from './UpdatePassword';
import Level2ReferralTree from './Level2ReferralTree';
import Level1ReferralTree from './Level1ReferralTree';
import Stage1ReferralTree from './Stage1ReferralTree';
import Stage2ReferralTree from './Stage2ReferralTree';
import Stage3ReferralTree from './Stage3ReferralTree';
import Stage4ReferralTree from './Stage4ReferralTree';
import Stage5ReferralTree from './Stage5ReferralTree';
import Stage6ReferralTree from './Stage6ReferralTree';
import Stage7ReferralTree from './Stage7ReferralTree';
import Stage8ReferralTree from './Stage8ReferralTree';
import Stage9ReferralTree from './Stage9ReferralTree';
import CompleteReferralTree from './CompleteReferralTree';
import StageWallet from './StageWallet';
import StageWalletPay from './StageWalletPay';
import StageWalletPaidStatus from './StageWalletPaidStatus';
import WalletPassBook from './WalletPassBook';
import StageReturnOfInvestment from './StageReturnOfInvestment';
import LeadTreeLevelStatus from './LeadTreeLevelStatus';
import OldReferralTree from './OldReferralTree';


class Hamburger extends Component {



  render() {

    return (

      <Router>
        <div className="AppHam">
          
          <SideBar pageWrapId={"page-wrap"} outerContainerId={"AppHam"} />
          <SideBar />

          <div id="page-wrap">
            <Route path='/' exact component={SchemeForm} />
            <Route path='/sgj/schemeForm' exact component={SchemeForm} />
            <Route path='/sgj/referralTree' exact component={ReferralTree} />
            <Route path='/sgj/level2ReferralTree' exact component={Level2ReferralTree} />
			<Route path='/sgj/level1ReferralTree' exact component={Level1ReferralTree} />
			<Route path='/sgj/completeReferralTree' exact component={CompleteReferralTree} />
			<Route path='/sgj/stage1ReferralTree' exact component={Stage1ReferralTree} />
			<Route path='/sgj/stage2ReferralTree' exact component={Stage2ReferralTree} />
			<Route path='/sgj/stage3ReferralTree' exact component={Stage3ReferralTree} />
			<Route path='/sgj/stage4ReferralTree' exact component={Stage4ReferralTree} />
			<Route path='/sgj/stage5ReferralTree' exact component={Stage5ReferralTree} />
			<Route path='/sgj/stage6ReferralTree' exact component={Stage6ReferralTree} />
			<Route path='/sgj/stage7ReferralTree' exact component={Stage7ReferralTree} />
			<Route path='/sgj/stage8ReferralTree' exact component={Stage8ReferralTree} />
			<Route path='/sgj/stage9ReferralTree' exact component={Stage9ReferralTree} />
			<Route path='/sgj/walletPassBook' exact component={WalletPassBook} />
			<Route path='/sgj/stageWalletPay' exact component={StageWalletPay} />
			<Route path='/sgj/stageWalletPaidStatus' exact component={StageWalletPaidStatus} />
			<Route path='/sgj/stageReturnOfInvestment' exact component={StageReturnOfInvestment} />
			 <Route path='/sgj/wallet' exact component={StageWallet} />
            <Route path='/sgj/walletPaidHistory' exact component={WalletPaidHistory} />
            <Route path='/sgj/walletTopUpRequest' exact component={WalletTopupRequestForm} />            
            <Route path='/sgj/walletRequest' exact component={WalletRequest} /> 
            <Route path='/sgj/memberSearch' exact component={MemberSearch} />          
            <Route path='/sgj/walletPay' exact component={WalletPay} />
            <Route path='/sgj/changePassword' component={ChangePassword} />      
			<Route path='/sgj/leadTree' component={LeadTreeLevelStatus} /> 
			<Route path='/sgj/oldTree' component={OldReferralTree} />   			
			
          </div>


        </div>
      </Router>

    )

  }
}

export default Hamburger;


