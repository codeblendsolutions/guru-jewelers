import React, { PureComponent } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import SweetAlert from 'react-bootstrap-sweetalert';
import Popup from "reactjs-popup";
import Tree from 'react-d3-tree';
import "./App.css";
import * as myConstClass from './Constants.js';
import LoadingOverlay from 'react-loading-overlay';

//import matchSorter from 'match-sorter'

class ChangePassword extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
          currentPassword: '',
          mobile:'',
          schemeId: '',
          newPassword: '',
		   firstName: '',
		    lastName: '',
		  schemeIdDisable: false,
          confirmPassword:'',
          userDetail:{
            schemeUserDetails:"",
            userAddressDetails:"",
            userBankDetails:"",
            schemeWalletDetails:""
          },
          validation:'',
          alert:null,
          overlayVisible: false
        }
    }
	
	 componentDidMount() {
		 
		 if(localStorage.getItem("customerType") === 'customer'){			
				this.setState({
					schemeId: localStorage.getItem('schemeId'),
					schemeIdDisable: true
				});
				
				
				var url = myConstClass.SCHEME_MEMBER_DETAIL+localStorage.getItem('schemeId').toUpperCase();      
        console.log("url : " + url);
        Axios.get(url)
            .then((response) => {
                this.setState({                  
                  userDetail: response.data,
                  currentPassword: response.data.schemePackageDetails.schemePassword,
				  firstName: response.data.schemeUserDetails.firstName,
				  lastName: response.data.schemeUserDetails.lastName,
                  mobile:response.data.schemeUserDetails.mobile  
                  
                })
                response.data.schemeWalletDetails != null? this.setState({balance:response.data.schemeWalletDetails.availableBalance}) : this.setState({balance:0})
            })
            .catch(error => {
                console.log(error);                
                this.setState({
                  name: "",
                  balance: 0
                }
                )
            });
		 }		
		
	 }
	
    changeHandler = (e) => {
        this.setState({
          [e.target.name]: e.target.value
        })
      }
      hideAlert(){
        this.setState({
          alert: null
        });
      }
    
    blurHandler = async(e) => {
        const name = e.target.name;
  const value = e.target.value;
      var response;
  if(name=="schemeId" && localStorage.getItem("customerType") === 'admin'){
        var url = myConstClass.SCHEME_MEMBER_DETAIL+String(value).toUpperCase();      
        console.log("url : " + url);
        await Axios.get(url)
            .then((response) => {
                this.setState({                  
                  userDetail: response.data,
                  currentPassword: response.data.schemePackageDetails.schemePassword,
				  firstName: response.data.schemeUserDetails.firstName,
				  lastName: response.data.schemeUserDetails.lastName,
                  mobile:response.data.schemeUserDetails.mobile  
                  
                })
                response.data.schemeWalletDetails != null? this.setState({balance:response.data.schemeWalletDetails.availableBalance}) : this.setState({balance:0})
            })
            .catch(error => {
                console.log(error);                
                this.setState({
                  name: "",
                  balance: 0
                }
                )
            });
            
    }
}  
submitHandler = async(e) => {  
    if((String(this.state.newPassword).length>5) && (String(this.state.newPassword)==(String(this.state.confirmPassword))))
    { 
      this.setState({overlayVisible: true})
         const params =
				{
				  schemeId: String(this.state.schemeId).toUpperCase(),
				  schemePassword: this.state.currentPassword,
				  schemeNewPassword: this.state.newPassword,
				  confirmNewPassword: this.state.confirmPassword,
				  firstName : this.state.firstName,
				  lastName : this.state.lastName
				}
    
    var url = myConstClass.UPDATE_PASSWORD;

     Axios.post(url, params).then(response => {
       console.log("response "+response);
       console.log("response data : "+response.data);
       //this.state.validation = response.data;
       this.setState({
        validation: response.data
       })
       console.log("validated : " + this.state.validation.validated);

       console.log("After Axios" +this.state.validation);

       
       var success = "Success  - "+ this.state.validation.message;
       var error =  "Error !  "+ this.state.validation.message;

       console.log(success);
       console.log(error);


       if (this.state.validation.validated === true) {
                  const getAlert = () => (
            <SweetAlert 
              success 
              title="Password Updated"
              onConfirm={() => this.hideAlert()}
              value={success}
            >
            </SweetAlert>
          );
          this.setState({overlayVisible: false})
          this.setState({
            alert: getAlert() 
          });
          document.getElementById('schemeId').focus();
          document.getElementById('paid').focus();
          this.state.newPassword="";
          this.state.confirmPassword="";
       }
       else {
           console.log("Update failed : " +error);
         /*  this.setState({
            showErrorMessage: true,
            successMessage : this.state.validation.message

          }) */

          const getAlert = () => (
            <SweetAlert 
              error 
              title={error} 
              onConfirm={() => this.hideAlert()}
              value={error}
            >
              
            </SweetAlert>
          );
          this.setState({overlayVisible: false})
          this.setState({
            alert: getAlert() 
          });
          
       }
     }).catch(error => {
       console.log(error);
       const getAlert = () => (
        <SweetAlert 
          error 
          title="Update failed!" 
          onConfirm={() => this.hideAlert()}
          value={error}
        >
          
        </SweetAlert>
      );
      this.setState({overlayVisible: false})
      this.setState({
        alert: getAlert() 
      });
      
      });
  }
  else{
    var title='';
    if(String(this.state.newPassword).length<6)
        title = "Minimum password length is 6!!"
    else
        title = "New password and confirm password does not match!!"

    const getAlert = () => (
      <SweetAlert 
        error 
        title={title }
        onConfirm={() => this.hideAlert()}
        value=""
      >
        
      </SweetAlert>
    );
    this.setState({
      alert: getAlert() 
    });
  }
}
       

    render(){
        const  { schemeId, currentPassword, newPassword, firstName, lastName, confirmPassword } = this.state;
        return(            
              <LoadingOverlay
  active={this.state.overlayVisible}
  spinner
  text='Processing...'
  >
    <div id="walletPay">
                <h1 style={{ color: 'maroon' }}>Change User Details</h1>
                <div class="block">
                <label style={{display:"inline-block", width: "190px", textAlign:"left", fontSize:"20px"}}>Scheme ID &nbsp; &nbsp;</label>
                <input style={{fontSize:"20px"}}  id="schemeId" type="text" name="schemeId" disabled={this.state.schemeIdDisable} placeholder="" value={schemeId}   onChange={this.changeHandler} onBlur={this.blurHandler}/>
                </div>
				<div>&nbsp;</div>
                <div class="block">
                <label style={{display:"inline-block", width: "190px", textAlign:"left",fontSize:"20px"}}>First Name &nbsp; &nbsp;</label>
                <input style={{fontSize:"20px"}} type="text" name="firstName" placeholder="" value={firstName}   onChange={this.changeHandler}/>
                </div>
                <div>&nbsp;</div>
				<div>&nbsp;</div>
                <div class="block">
                <label style={{display:"inline-block", width: "190px", textAlign:"left",fontSize:"20px"}}>Last Name &nbsp; &nbsp;</label>
                <input style={{fontSize:"20px"}} type="text" name="lastName" placeholder="" value={lastName}   onChange={this.changeHandler}/>
                </div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div class="block">
                <label style={{display:"inline-block", width: "190px", textAlign:"left", fontSize:"20px"}}>Current password &nbsp; &nbsp;</label>
                <input style={{fontSize:"20px"}} disabled={localStorage.getItem("customerType") === 'admin'} type="text" name="currentPassword" placeholder="" value={currentPassword}   onChange={this.changeHandler}/>
                </div>
                <div>&nbsp;</div>
                <div class="block">
                <label style={{display:"inline-block", width: "190px", textAlign:"left",fontSize:"20px"}}>New password &nbsp; &nbsp;</label>
                <input style={{fontSize:"20px"}} type="text" name="newPassword" placeholder="" value={newPassword}   onChange={this.changeHandler}/>
                </div>
                <div>&nbsp;</div>
                <div class="block">
                <label style={{display:"inline-block", width: "190px", textAlign:"left",fontSize:"20px"}}>Confirm password</label>
                <input style={{fontSize:"20px"}} id="paid" type="text" name="confirmPassword" placeholder="" value={confirmPassword}   onChange={this.changeHandler}/>
                </div>
                <div>&nbsp;</div>
                <div>
                    <button className="schemePageButton" style={{fontSize:"22px", width:"120px", height:"50px", fontWeight:"600"}} onClick = {this.submitHandler} >Submit</button>
                    </div>
                    {this.state.alert}
                    <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                </div>
                    </LoadingOverlay>
            

        )
    }
}
export default ChangePassword;