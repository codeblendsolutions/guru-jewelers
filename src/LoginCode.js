import React, { Component } from 'react';
import './LoginBasic.css';
import Axios from 'axios';
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import * as myConstClass from './Constants.js';
import LoadingOverlay from 'react-loading-overlay';

class LoginCode extends React.Component {


    initialState = {
        sgjId: '',
        phone: '',
        password: '',
        showErrorMessage: false,
        isLoggedIn: false,
        validation: []
    };

    initalParams = {
        sgjId: '',
        phone: '',
        password: ''
    }
    constructor(props) {
        super(props);



        this.state = {
            overlayVisible:false,
            sgjId: '',
            phone: '',
            password: '',
            showErrorMessage: false,
            isLoggedIn: false,
            validation: [],
            showLoader: false
        }

    }



    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }



    handleOnSubmit = (e) => {

        e.preventDefault();

        this.setState({
            showLoader: false,
            overlayVisible:true        })
        const params = {
        //    sgjId: this.state.sgjId,
            // phone: this.state.phone,
          //  password: this.state.password

            
                "schemePackageDetails": {
                    "schemeId": this.state.sgjId,
                    "schemePassword": this.state.password
                }
            
        }

        console.log("Before Axios");

        const url = myConstClass.LOGIN_URL;
        console.log("urlConst : " + url);

        Axios.post(url, params)
            .then(response => {

                this.setState({
                    overlayVisible:false,
                    showLoader: false
                })
                this.state.validation = response.data;
                console.log("validated : " + this.state.validation.validated);
                console.log("customer Type : " + this.state.validation.customerType);

                console.log("After Axios");
                if (this.state.validation  != null && this.state.validation.validated) {
					
                    console.log("Welcome validated");
					
                    localStorage.setItem('isLoggedIn', true);
                    localStorage.setItem('schemeId', this.state.sgjId.toUpperCase());
                    localStorage.setItem('customerType', this.state.validation.customerType);
					 localStorage.setItem('walletTopUpAmount', this.state.validation.walletTopUpAmount);
					 localStorage.setItem('level1Completed', this.state.validation.level1Completed);
					  localStorage.setItem('level2Completed', this.state.validation.level2Completed);
					   localStorage.setItem('level3Completed', this.state.validation.level3Completed);
					   localStorage.setItem('level4Completed', this.state.validation.level4Completed);
					  localStorage.setItem('level5Completed', this.state.validation.level5Completed);
					   localStorage.setItem('level6Completed', this.state.validation.level6Completed);
					   localStorage.setItem('level7Completed', this.state.validation.level7Completed);
					  localStorage.setItem('level8Completed', this.state.validation.level8Completed);
					   localStorage.setItem('level9Completed', this.state.validation.level9Completed);
					
                    if (this.state.validation.customerType === 'admin') {
                        this.props.history.push('/sgj/schemeForm');
                    } else {
                        this.props.history.push('/sgj/stage1ReferralTree');
                    }

                 //   localStorage.setItem('availableSchemeAmount', this.state.validation.availableWalletBalance);

                   // console.log("ajshjh : "+ localStorage.getItem('availableSchemeAmount'));
                    this.state = this.initialState;
                    //  params = this.initalParams;
                    window.location.reload();

                }
                else {
                    console.log("sorry not  validated");
                    localStorage.setItem('isLoggedIn', false);
                    this.setState({
                        showErrorMessage: true,
                        isLoggedIn: true
                    })
                }

            })
            .catch(error => {
                console.log("Error Axios");
                console.log(error);
                this.setState({
                    showLoader: false
                })
            });






    };
    render() {
        return (
            <div style={{ backgroundImage: "url(./images/background1.jpg)", height: "100vh" }}>
                  <LoadingOverlay
  active={this.state.overlayVisible}
  spinner
  text='Logging you in...'
  >
                <form onSubmit={this.handleOnSubmit}>
                    <div><br></br></div>
                    <img src="./images/coin.png" alt="coin" className="sgjCoinLogin"/>
                    <h4 style={{color:'white'}}>Welcome Back!</h4>
                    {this.state.showErrorMessage ?
                        <div><h1 style={{
                            color: 'red'
                        }}> Credentials are not Valid</h1>
                        </div>
                        : null}
                    <div className='form-group row'>
                        <input className='input' type='text' name='sgjId' onChange={this.handleChange} placeholder='Please Enter SGJID' />
                    </div>
                    <div className='form-group row'>
                        <input className='input' type='password' name='password' onChange={this.handleChange} placeholder='Password' />
                    </div>
                    <div className='form-group row'>
                        <button className='btn' type='submit'>Log In</button>
                    </div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                </form>
                <div className='loader-spinner' >
                    <Loader type="Plane" color="#00BFFF" visible={this.state.showLoader} />
                </div>
                </LoadingOverlay>
            </div>
        )
    }
}

export default LoginCode;