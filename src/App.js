import React from 'react';
import './App.css';
import About from './About';
import Home from './Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Hamburger from './Hamburger';
import LoginCode from './LoginCode';
import NavClass from './NavClass';



function App() {
  return (
//<img src={'/images/Maintenance.png'} />
  <Router>
      <div className="App">
        <NavClass />
        <Route path='/' exact component={Home} />
        <Route path='/home' component={Home} />
        <Route path='/about' component={About} />
        <Route path='/login' component={LoginCode} />
        <Route path='/sgj/schemeForm' component={Hamburger} />
        <Route path='/sgj/referralTree' component={Hamburger} />
		<Route path='/sgj/stage1ReferralTree' component={Hamburger} />
        <Route path='/sgj/level2ReferralTree' component={Hamburger} />
		<Route path='/sgj/level1ReferralTree' component={Hamburger} />
        <Route path='/sgj/wallet' component={Hamburger} />
		<Route path='/sgj/stageWalletPay' component={Hamburger} />
		<Route path='/sgj/stageWalletPaidStatus' component={Hamburger} />
        <Route path='/sgj/walletPaidHistory' component={Hamburger} />
		 <Route path='/sgj/stageReturnOfInvestment' component={Hamburger} />
        <Route path='/sgj/memberSearch' component={Hamburger} />
        <Route path='/sgj/walletTopUpRequest' component={Hamburger} />
        <Route path='/sgj/walletRequest' component={Hamburger} />
        <Route path='/sgj/walletPay' component={Hamburger} />      
        <Route path='/sgj/changePassword' component={Hamburger} />      
		<Route path='/sgj/leadTree' component={Hamburger} />      
		<Route path='/sgj/oldTree' component={Hamburger} />    		
      </div>
    </Router> 
  );
}

export default App;

