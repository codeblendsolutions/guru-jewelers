import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-accessible-accordion/dist/fancy-example.css';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Slide from '@material-ui/core/Slide';
import SweetAlert from 'react-bootstrap-sweetalert';

import ExportToExcel from './ExportToExcel';

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
class LeadTreeLevelStatus extends Component {

    constructor(props) {
        super(props)
        this.state = {
            availableBalance: '',
            isDialogOpen: false,
            schemeId:'',
			userId: '',
            amount:'',
            alert:null,
			startDate:'',
			toDate:'',
			stage2CustomerWallet: [],
			stage3CustomerWallet: [],
			stage4CustomerWallet: [],
			stage5CustomerWallet: [],
			stage6CustomerWallet: [],
			stage7CustomerWallet: [],
			stage8CustomerWallet: [],
			stage9CustomerWallet: [],
			stage1CustomerWallet:[],
			leadStage1Status:[],
			leadStage2Status:[],
			leadStage3Status:[],
			leadStage4Status:[],
			leadStage5Status:[],
			leadStage6Status:[]
        
		}
    }
	
	
	
	changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
	
    handlePay = (e) => {
		
		
		
        const getAlert = () => (
            <SweetAlert
              success
              title={"Payment updated successfully!"}
              onConfirm={() => this.hideAlert()}
              value={"Payment Updated"}
            />
            
          );
       this.setState(
	   {alert:getAlert(),
    isDialogOpen:false});
      }
	  
	  
	  
	  
	 dateFilterLevel1 = (e) => {
		  console.log( 'dateFilterSubmit1');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }
	 
 dateFilterLevel2 = (e) => {
		  console.log( 'dateFilterSubmit2');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level2'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage2CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel3 = (e) => {
		  console.log( 'dateFilterSubmit3');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level3'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage3CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel4 = (e) => {
		  console.log( 'dateFilterSubmit4');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level4'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage4CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel5 = (e) => {
		  console.log( 'dateFilterSubmit5');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level5'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage5CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel6 = (e) => {
		  console.log( 'dateFilterSubmit6');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level6'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage6CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel7 = (e) => {
		  console.log( 'dateFilterSubmit7');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level7'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage7CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel8 = (e) => {
		  console.log( 'dateFilterSubmit8');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level8'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage8CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel9 = (e) => {
		  console.log( 'dateFilterSubmit9');
		   const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level9'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage9CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }	  
	  
	  
	  stage1Click = (e) => {
		console.log( "Stage 1 clicked");
		
		const url = myConstClass.LEAD_STAGE1_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage1Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
      }
	  stage2Click = (e) => {
		console.log( "Stage 2 clicked");
		const url = myConstClass.LEAD_STAGE2_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage2Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
      }

	  stage3Click = (e) => {
		console.log( "Stage 3 clicked");
		const url = myConstClass.LEAD_STAGE3_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage3Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
      }

	  stage4Click = (e) => {
		console.log( "Stage 4 clicked");
		const url = myConstClass.LEAD_STAGE4_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage4Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
      }

	  stage5Click = (e) => {
		console.log( "Stage 5 clicked");
		const url = myConstClass.LEAD_STAGE5_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage5Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
      }

	  stage6Click = (e) => {
		console.log( "Stage 6 clicked");
		const url = myConstClass.LEAD_STAGE6_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage6Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
      }

	  stage7Click = (e) => {
		console.log( "Stage 7 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level7'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage7CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage8Click = (e) => {
		console.log( "Stage 8 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level8'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage8CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage9Click = (e) => {
		console.log( "Stage 9 clicked");
		this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level9'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage9CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }	  
	  
    hideAlert=() =>{
        this.setState({alert:null});
    }
    handleClose = () => {
        this.setState({
            isDialogOpen:false
          })
      };
      handleOpen = (e) => {
          console.log(e.target.value);
          var i = this.state.stage1CustomerWallet.find(item=>{
              return item.schemeId===e.target.value
          })
        if(i){
        this.setState({
            isDialogOpen:true,
            schemeId:i.schemeId,
            amount:i.stageAmount         
          })
        }
      };
	  
	  handleAddClick = () => {
        
		
		console.log("sdafa"+this.state.userId);
		
		if(this.state.userId === ""){
		  const getAlert = () => (
            <SweetAlert
              error
              title={"Please Enter User ID to Proceed!"}
              onConfirm={() => this.hideAlert()}
              value={"Please Enter User ID"}
            />
            
          );
		  
       this.setState({
			alert:getAlert(),
			isDialogOpen:false
			
			});
		} else {
			
			const url = myConstClass.LEAD_STAGE1_STATUS+this.state.userId;
			
			Axios.get(url).then(response => {      
				this.setState({
					leadStage1Status: response.data
				})
			}).catch(error => {
				console.log(error.message);
			});
			
		}
	
	
      };
        
    componentDidMount() {
        
		/*
		var schemeID = localStorage.getItem('schemeId');
        const url = myConstClass.STAGE_WALLET_PAID_DETAILS;
		const params =
		   {
        
          fromDate: '',
          toDate: '',
		   stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
		
         */
			
			
    }
   
    rowWalletPay(original){
        //return(<span></span>);
        return(
		(original.stageWalletStatus) ?(<span>{original.stageWalletStatus}</span>):(<button padding="5px" borderRadius="5px" value={original.schemeId} onClick={this.handleOpen} >Pay</button>));
    };


    render() {
		
		 const {  userId } = this.state;
		
        const columns = [
            {
                Header: "Scheme ID",
                accessor: "schemeID"
            },
			{
                Header: "First Name",
                accessor: "firstName"
            },
            {
                Header: "Completed Date",
                accessor: "completedDate"
            },
			{
				id:'levelCompleted',
				 Header: "Stage Completed ?",
                accessor: d => d.levelCompleted.toString()
				
			}
        ];
		
const innerColumns = [
    {
      id: "bookingId",
      Header: 'Scheme ID',
      accessor: 'name',
    },  // String-based value accessors!
    {
      Header: "FirstName", // Custom header components!
      accessor: "value"
    }
  ];
	
	  const {alert}=this.state;
        return (
            <div id="wallet">
                <h1>Lead Level History</h1>
				
				<div>
                  <label>Lead User ID<span style={{ color: "red" }}>*</span></label>
               <input type="text" name="userId" value={userId} placeholder="Lead User Id " onChange={this.changeHandler} />
				
				
                  <Button variant="contained" color="primary" allign="center" style={{ height: 30, fontSize: "90%", opacity: "1" }} onClick={this.handleAddClick}>Add</Button>
                  <br /><br /> &nbsp;&nbsp;
                 
               
                </div>
				
				<Accordion allowZeroExpanded preExpanded={['expire']}>
            <AccordionItem uuid="expire">
                <AccordionItemHeading onClick={this.stage1Click}>
                    <AccordionItemButton>
                       STAGE 1
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
				
    
				<ReactTable
                    columns={columns}
                    data={this.state.leadStage1Status}
					defaultPageSize={10}
					filterable
					
					SubComponent={(v) =>
              this.state.leadStage1Status[v.row._index]["referrList"] && (<ReactTable
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: '#dadada',
                  borderRadius: '8px',

                }}
                minRows={1}
                showPaginationBottom={false}
                data={this.state.leadStage1Status[v.row._index]["referrList"]}
                columns={this.state.leadStage1Status[v.row._index]["referrList"] ? innerColumns : [{ Header: "hello" }]} />)
				
				}				
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
			

            <AccordionItem>
                <AccordionItemHeading onClick={this.stage2Click}>
                    <AccordionItemButton  >
                        STAGE 2
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   
				    
    
        <ReactTable
                    columns={columns}
                    data={this.state.leadStage2Status}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
					SubComponent={(v) =>
              this.state.leadStage2Status[v.row._index]["referrList"] && (<ReactTable
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: '#dadada',
                  borderRadius: '8px',

                }}
                minRows={1}
                showPaginationBottom={false}
                data={this.state.leadStage2Status[v.row._index]["referrList"]}
                columns={this.state.leadStage2Status[v.row._index]["referrList"] ? innerColumns : [{ Header: "hello" }]} />)
				
				}	
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				   
				   
				 
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage3Click}>
                    <AccordionItemButton>
                        STAGE 3
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  
				    
    
        <ReactTable
                    columns={columns}
                    data={this.state.leadStage3Status}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
					SubComponent={(v) =>
              this.state.leadStage3Status[v.row._index]["referrList"] && (<ReactTable
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: '#dadada',
                  borderRadius: '8px',

                }}
                minRows={1}
                showPaginationBottom={false}
                data={this.state.leadStage3Status[v.row._index]["referrList"]}
                columns={this.state.leadStage3Status[v.row._index]["referrList"] ? innerColumns : [{ Header: "hello" }]} />)
				
				}	
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				  
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage4Click}>
                    <AccordionItemButton>
                        STAGE 4
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  
				    
    
        <ReactTable
                    columns={columns}
                    data={this.state.leadStage4Status}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >

{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage5Click}>
                    <AccordionItemButton>
                        STAGE 5
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   
				      
    
        <ReactTable
                    columns={columns}
                    data={this.state.leadStage5Status}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage6Click}>
                    <AccordionItemButton>
                        STAGE 6
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   
				       
    
        <ReactTable
                    columns={columns}
                    data={this.state.leadStage6Status}
				defaultPageSize={10}
				noDataText={"People are yet to complete the previous stage"}
					filterable
					
                >


				{(state,filteredData,instance)=> {
					this.reactTable = state.pageRows.map(post=> { return post._original});
					return (
					<div>
					{filteredData()}
					<ExportToExcel posts={this.reactTable}/>
					</div>
					)
				}}

                </ReactTable>
				
                </AccordionItemPanel>
            </AccordionItem>
				
            
				
        </Accordion>
        <Dialog
        open={this.state.isDialogOpen}
        //TransitionComponent={Transition}
        keepMounted
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{"Wallet Pay"}</DialogTitle>
        <DialogContent>
        <div align="left" style={{marginLeft:"",display: 'flex', flexWrap:"wrap"}}>       
        <TextField
          id="standard-number"
          label="Scheme ID"
          value={this.state.schemeId || ''}  
          InputProps={{readOnly: true,}}  
          style={{width: '25ch', margin:"1em"}}                
        /> 
         <TextField
          id="standard-number"
          label="Amount"
          value={this.state.amount || ''}  
          InputProps={{readOnly: true,}}  
          style={{width: '25ch', margin:"1em"}}                   
        /> 
        </div >
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handlePay} color="primary">
            Pay
          </Button>
          <Button onClick={this.handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      {this.state.alert}
            </div>

        )

    }
}

export default LeadTreeLevelStatus;


