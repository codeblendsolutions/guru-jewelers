import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';

class WalletPaidHistory extends Component {

    constructor(props) {
        super(props)
        this.state = {
            // availableBalance: '',
            paidhistory: []
        }

    }


    componentDidMount() {


        var schemeID = localStorage.getItem('schemeId');

        const url = myConstClass.WALLET_PAID_HISTORY + schemeID;

        console.log("url : " + url);
        Axios.get(url)
            .then((response) => {
                console.log("respoinse : " + response);
                console.log("response data : " + response.data);
                console.log("response dnullata : " + response.data === null);
                console.log("response length : " + response.data.length);
                if (response.data.length > 0) {
                    this.setState({
                        paidhistory: response.data
                    })
                }
                console.log("paidhistory : " + this.state.paidhistory);

            })

            .catch(error => {
                console.log(error);
            });
    }


    render() {
        const columns = [
            {
                Header: "Paid Date",
                accessor: "paidDate"
            },
            {
                Header: "Paid Amount",
                accessor: "paidAmount"
            },
            {
                Header: "Previous Balance",
                accessor: "previousBalance"
            },
            {
                Header: "Updated Balance",
                accessor: "updatedBalance"
            },
            {
                Header: "Mode of Payment",
                accessor: "modeOfPayment"
            }
        ];

        const adminColumns = [
            {
                Header: "Scheme ID",
                accessor: "schemeId"
            },
            {
                Header: "Paid Date",
                accessor: "paidDate"
            },
            {
                Header: "Paid Amount",
                accessor: "paidAmount"
            },
            {
                Header: "Previous Balance",
                accessor: "previousBalance"
            },
            {
                Header: "Updated Balance",
                accessor: "updatedBalance"
            },
            {
                Header: "Mode of Payment",
                accessor: "modeOfPayment"
            }
        ];


        return (
            <div id="wallet">
                <h1>Wallet Paid History</h1>
                {localStorage.getItem("customerType") === 'admin' ?
                    <ReactTable

                        columns={adminColumns}
                        data={this.state.paidhistory}
                    >
                    </ReactTable>
                    :

                    <ReactTable

                        columns={columns}
                        data={this.state.paidhistory}
                    >
                    </ReactTable>
                }
                
            </div>

        )

    }
}

export default WalletPaidHistory;


