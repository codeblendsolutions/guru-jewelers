import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';
import SweetAlert from 'react-bootstrap-sweetalert';
import LoadingOverlay from 'react-loading-overlay';

class WalletTopupRequestForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            amount: '',
            modeOfPayment: '',
            walletRequestType: 'select',
            errorMessage: '',
            showErrorMessage: false,
            formValid: false,
            topUpHistory: [],
            validation: [],
            alert:null,
            overlayVisible:false,
            availableSchemeAmount:''
        }




        this.onRadioChange = this.onRadioChange.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }
    hideAlert(){
        this.setState({
          alert: null
        });
      }

    onRadioChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })

    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    submitHandler = async(e) => {
        e.preventDefault();
        var fValid=false;
        console.log("welcom submit");

        console.log("state : " + this.state.walletRequestType + "- " + this.state.amount + "-" + this.state.modeOfPayment + "-" + localStorage.getItem('schemeId'));

        if (this.state.walletRequestType === "select") {
            this.setState({
                showErrorMessage: true,
                formValid: false,
                errorMessage: 'Please select a type from dropdown'
            })
        } else if (this.state.amount === "") {

            this.setState({
                showErrorMessage: true,
                formValid: false,
                errorMessage: 'Please enter amount to proceed'
            })
            
        } else if (!(this.state.modeOfPayment === 'cash' || this.state.modeOfPayment === 'bank')) {
            this.setState({
                showErrorMessage: true,
                formValid: false,
                errorMessage: 'Please select a mode of Payment'
            })
        } else {

            this.setState({
                showErrorMessage: false,
                formValid: true,
                errorMessage: ''
            });
            
            fValid=true;
        }

        console.log("formValid : "+this.state.formValid);
        if (fValid) {
            this.setState({overlayVisible: true})
            const params =

            {
                type: this.state.walletRequestType,
                amount: this.state.amount,
                paymentMode: this.state.modeOfPayment,
                schemeId: localStorage.getItem('schemeId'),
				adminSchemeID:'SGJNS00603'
            }


            console.log("params : " + params);
            console.log("params : " + params.type + "- " + params.amount + "-" + params.paymentMode + "-" + params.schemeId);

            const url =  myConstClass.WALLET_TOPUP;
            console.log("url : " + url);
    
         await Axios.post(url, params).then(response => {
           console.log("response "+response);
           console.log("response data : "+response.data);
           //this.state.validation = response.data;
           this.setState({
            validation: response.data
           })
           console.log("validated : " + this.state.validation.validated);
    
           console.log("After Axios");
    
           
           var success = "Success  - "+ this.state.validation.message;
           var error =  "Error !  "+ this.state.validation.message;
    
           console.log(success);
           console.log(error);
         console.log("mobileNumber ID : "+this.state.validation.mobileNumber);

         console.log("amount ID : "+this.state.amount);

    
    
    
           if (this.state.validation.validated === true) {
               console.log("WElcome validated");


/*
               const MSG91 = require('msg91');
          this.msg91 = require("msg91")("292308AgXOs1Sm5d6dfe1c", "SGJWLR", 4 );
          this.mobileNo = this.state.validation.mobileNumber;
          this.msg91.getBalance(4, function(err, msgCount){
              console.log(err);
              console.log(msgCount);
          });
  
          var message = "Scheme ID : "+localStorage.getItem('schemeId')+", has requested for "+this.state.amount+" rs, kindly Approve/ Reject."
          this.msg91.send(this.mobileNo, message , function(err, response){
              console.log(err);
              console.log(response);
          });
*/

               const getAlert = () => (
                <SweetAlert 
                  success 
                  title={"Request submitted successfully!"}
                  onConfirm={() => this.hideAlert()}
                  value={"sucess"}
                >
                </SweetAlert>
              );
              this.setState({overlayVisible: false})
              this.setState({
                alert: getAlert() 
              });
              this.componentDidMount();
           }
           else{
            const getAlert = () => (
                <SweetAlert 
                  error 
                  title={error}
                  onConfirm={() => this.hideAlert()}
                  value={error}
                >
                </SweetAlert>
              );
              this.setState({overlayVisible: false})
              this.setState({
                alert: getAlert() 
              });
           }  
        }).catch(error => {
            console.log(error);
          });
        }
    }
    componentDidMount() {


        var schemeID = localStorage.getItem('schemeId');

        const url = myConstClass.WALLET_TOPUP_HISTORY + schemeID;

        console.log("url : " + url);
        Axios.get(url)
            .then((response) => {
                console.log("respoinse : " + response);
                console.log("response data : " + response.data);
                console.log("response dnullata : " + response.data != null);
             //   console.log("response length : " + response.data.length);
                if (response.data != null) {
                    this.setState({
                        topUpHistory: response.data,
                    //    availableSchemeAmount: response.data.availableSchemeAmount
                    })
                }
                console.log("topUpHistory : " + this.state.topUpHistory);

            })

            .catch(error => {
                console.log(error);
            });
    }


    render() {
        const { amount, modeOfPayment, walletRequestType } = this.state;
        const columns = [
            {
                Header: "Amount",
                accessor: "amount"
            },
            {
                Header: "State",
                accessor: "state",
                Cell: e =><span> {e.value==null?"":e.value[0].toUpperCase()+e.value.slice(1)} </span>
            },
            {
                Header: "Requested",
                accessor: "requestedDate",
                id:"requestedDate"
            },
            {
                Header: "Approved/Rejected",
                accessor: "authorizedDate"
            },
            {
                Header: "Mode of Payment",
                accessor: "paymentMode"
            }
        ];
        return (


            <LoadingOverlay
  active={this.state.overlayVisible}
  spinner
  text='Processing...'
  >
            <div id="walletTopUp">
                <h1>Wallet Top Up Request</h1>
                <h2>Available Scheme TopUp Amount :  {localStorage.getItem("walletTopUpAmount")}</h2>
                {this.state.showErrorMessage ?
                    <div><h3 style={{
                        color: 'red'
                    }}> {this.state.errorMessage}</h3>
                    </div>
                    : null}
                <form onSubmit={this.submitHandler}>

                    <label>Type<span style={{ color: "red" }}>*</span></label>
                    
                    <select name="walletRequestType" onChange={this.changeHandler} value={this.state.walletRequestType}>
                        <option value="select">Select</option>
                        <option value="request">Request</option>
                    </select>


                    <label>&nbsp;&nbsp;Amount<span style={{ color: "red" }}>*</span></label>

                    <input type="number" name="amount" placeholder="Please enter amount " value={amount} onChange={this.changeHandler} />


                    <label>&nbsp;&nbsp;Mode Of Payment<span style={{ color: "red" }}>*</span></label>

                    <input type="radio" name="modeOfPayment" onChange={event => this.onRadioChange(event)} value="cash" /> Cash
              <input type="radio" name="modeOfPayment" onChange={event => this.onRadioChange(event)} value="bank" /> Bank&nbsp;&nbsp;
              <button className="walletTopUpButton" type="submit" >Submit</button>
                </form>


                <div>&nbsp;</div>
                <ReactTable
                style={{
                    //width: '100%',
                    backgroundColor: 'HONEYDEW' ,
                    display: 'flex',
                    justifyContent:'center',
                    align:'middle'                   
                  }}
                    filterable
                    defaultFilterMethod={(filter, row) =>
                        (String(row[filter.id])).toLowerCase().startsWith((filter.value).toLowerCase())}
                    className="-striped -highlight"
                    columns={columns}
                    data={this.state.topUpHistory}
                    defaultSorted={[
                        {
                          id: "requestedDate",
                          desc: true
                        }
                      ]}
                    defaultPageSize={15}
                >
                </ReactTable>
                {this.state.alert}
            </div>
            </LoadingOverlay>
        )

    }
}

export default WalletTopupRequestForm;


