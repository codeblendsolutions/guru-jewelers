import React, { PureComponent } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import SweetAlert from 'react-bootstrap-sweetalert';
import Popup from "reactjs-popup";
import Tree from 'react-d3-tree';
import "./App.css";
import * as myConstClass from './Constants.js';
import { request } from 'https';
import LoadingOverlay from 'react-loading-overlay';

class WalletRequest extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            requestDetails:[],
            open: false,
            selected:{},
            userDetail:{
                schemeUserDetails:"",
                userAddressDetails:"",
                userBankDetails:""
              },
              alert:null,
              overlayVisible:false
        }
        this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    }
    hideAlert(){
        this.setState({
          alert: null
        });
      }
    openModal() {
        this.setState({ open: true });
      }
      closeModal() {
        document.body.style.overflow = 'auto'
        this.setState({ open: false });
      }
    componentDidMount() {
      
        var url = myConstClass.WALLET_TOPUP_HISTORY+localStorage.getItem('schemeId');
      
        Axios.get(url)
            .then((response) => {               
                this.setState({                  
                    requestDetails: response.data                  
                })
               })
            .catch(error => {
                console.log(error);
            });
    }
    getMemberName = async(schemeID) =>{
        var url = myConstClass.SCHEME_FORM_URL+'/'+schemeID;
        Axios.get(url)
            .then((response) => {
                console.log(response.data.schemeUserDetails.firstName);
               
                    return response.data.schemeUserDetails.firstName;
              
            })

            .catch(error => {
                console.log(error);
                return null;
            });
    }

    rejectHandler = (e)=>{
        var flag = false;
        var i;
        var el = document.getElementsByName("walletCheck");
        for(i=0;i<el.length;i++){            
            if(el[i].checked==true)
            {
                flag = true;
            }
        }
        if(flag){
        const getAlert = () => (<SweetAlert
        input
        //showCancel
        cancelBtnBsStyle="default"
        title="Reason for rejection"
        placeholder=""
        onConfirm={(response) => {this.hideAlert(); this.submitReject(response);}}
        //onCancel={this.hideAlert}
        > </SweetAlert>);
        this.setState({
            alert: getAlert() 
          });
        }
        }
    submitReject= async(comment)=> {
        var el = document.getElementsByName("walletCheck");
        var i;
        var err ="";
        this.setState({overlayVisible: true})
       for(i=0;i<el.length;i++){            
            if(el[i].checked==true)
            {
              var pValue = String(el[i].value).split("~");                              
                    const params =    {
                       adminSchemeID:localStorage.getItem('schemeId'),
	                    type:"reject",
	                    amount:pValue[2],
	                    paymentMode:pValue[3],
	                    referenceNumber:pValue[4],
	                    comments:comment,
	                    schemeId:pValue[0],
	                    walletRequestId:pValue[1]
                    }
                    var url = myConstClass.WALLET_TOPUP;
                    await Axios.post(url, params).then(response => {
                         console.log("response "+response);
                        console.log("response data : "+response.data.message);
                        if(response.data.validated=== false)
                        {
                            err=err+response.data.message;
                         
                          }
if(response.data.validated=== true){
               
  const MSG91 = require('msg91');
  this.msg91 = require("msg91")("292308AgXOs1Sm5d6dfe1c", "SGJWLR", 4 );
  this.mobileNo = response.data.mobileNumber;
  this.msg91.getBalance(4, function(err, msgCount){
      console.log(err);
      console.log(msgCount);
  });

  var message = "Your request has been Rejected by admin."
  this.msg91.send(this.mobileNo, message , function(err, response){
      console.log(err);
      console.log(response);
  });
          
}





                    })
                    .catch(error =>{
                        console.log(error);
                        err=err+error;
                        console.log(err);
                    })
                
            }        
        }
        console.log(err);
        if(!err.length>0)
        {
            console.log(err.length);
            const getAlert = () => (
                <SweetAlert 
                  success 
                  title="Request(s) rejected successfully!"
                  onConfirm={() => this.hideAlert()}
                  value={"success"}
                >
                </SweetAlert>
              );
              this.setState({overlayVisible: false})
              this.setState({
                alert: getAlert() 
              });
              var el = document.getElementsByName("walletCheck");
              for(i=0;i<el.length;i++){            
                if(el[i].checked==true)
                {this.state.selected[el[i].id] = false;
                console.log(i);}
              }
        }
        else
        {
            const getAlert = () => (
                <SweetAlert 
                  error 
                  title={err}
                  onConfirm={() => this.hideAlert()}
                  value={"error"}
                >
                </SweetAlert>
              );
              this.setState({overlayVisible: false})
              this.setState({
                alert: getAlert() 
              });
        }
        this.componentDidMount();
    }
approveHandler = (e)=>{
      var flag = false;
      var i;
      var el = document.getElementsByName("walletCheck");
      for(i=0;i<el.length;i++){            
          if(el[i].checked==true)
          {
              flag = true;
          }
      }
      if(flag){
      const getAlert = () => (<SweetAlert
      input
      //showCancel
      cancelBtnBsStyle="default"
      title="Comments for approval"
      placeholder=""
      onConfirm={(response) => {this.hideAlert(); this.submitApprove(response);}}
      //onCancel={this.hideAlert}
      > </SweetAlert>);
      this.setState({
          alert: getAlert() 
        });
      }
      }
  submitApprove= async(comment)=> {
    this.setState({overlayVisible: true})
      var el = document.getElementsByName("walletCheck");
      var i;
      var err ="";
     for(i=0;i<el.length;i++){            
          if(el[i].checked==true)
          {
            var pValue = String(el[i].value).split("~");                              
                  const params =    {
                     adminSchemeID:localStorage.getItem('schemeId'),
                    type:"approve",
                    amount:pValue[2],
                    paymentMode:pValue[3],
                    referenceNumber:pValue[4],
                    comments:comment,
                    schemeId:pValue[0],
                    walletRequestId:pValue[1]
                  }
                  var url = myConstClass.WALLET_TOPUP;
                  await Axios.post(url, params).then(response => {
                       console.log("response "+response);
                      console.log("response data : "+response.data.message);
                      if(response.data.validated==false)
                      {
                          err=err+response.data.message;
                      }


                      if (response.data.validated === true) {
         /*      const MSG91 = require('msg91');
               this.msg91 = require("msg91")("292308AgXOs1Sm5d6dfe1c", "SGJWLR", 4 );
               this.mobileNo = response.data.mobileNumber;
               this.msg91.getBalance(4, function(err, msgCount){
                   console.log(err);
                   console.log(msgCount);
               });
       
               var message = "Your request has been successfully approved by admin. Please visit our website www.sri-gurujewellery.com to add more members."
               this.msg91.send(this.mobileNo, message , function(err, response){
                   console.log(err);
                   console.log(response);
               }); */

              }
                  })
                  .catch(error =>{
                      console.log(error);
                      err=err+error;
                      console.log(err);
                  })
              
          }        
      }
      console.log(err);
      if(!err.length>0)
      {
          console.log(err.length);
          const getAlert = () => (
              <SweetAlert 
                success 
                title="Request(s) approved successfully!"
                onConfirm={() => this.hideAlert()}
                value={"success"}
              >
              </SweetAlert>
            );
            this.setState({overlayVisible: false})
            this.setState({
              alert: getAlert() 
            });
            var el = document.getElementsByName("walletCheck");
            for(i=0;i<el.length;i++){            
              if(el[i].checked==true)
              {this.state.selected[el[i].id] = false;
              console.log(i);}
            }
      }
      else
      {
          const getAlert = () => (
              <SweetAlert 
                error 
                title={err}
                onConfirm={() => this.hideAlert()}
                value={"error"}
              >
              </SweetAlert>
            );
            this.setState({overlayVisible: false})
            this.setState({
              alert: getAlert() 
            });
      }
      this.componentDidMount();
  }


    handleOnClick = async(e) => {
        e.preventDefault();
        //var userDetail='';
        var url = myConstClass.SCHEME_MEMBER_DETAIL+'/'+e.target.innerText;      
        console.log("url : " + url);
        await Axios.get(url)
            .then((response) => {
                this.setState({                  
                  userDetail: response.data,
                  open: true
                })
            })
            .catch(error => {
                console.log(error);
            });
            document.body.style.overflow = 'hidden'
    }
   
      toggleRow(e,title) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[title] = !this.state.selected[title];
        this.setState({
            selected: newSelected
        });
        //console.log(e);
    }
    render(){
        const columns = [
            {
                Header: "",
                id:"checkbox",
                accessor: "schemeID",
                headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
                maxWidth: "100",
                //filterMethod: (filter, row) => row[filter.id].startsWith(filter.value),            
                    Cell: (e) => <input style={{width:"20px", height:"20px"}} type={"checkbox"}  name={"walletCheck"} checked={this.state.selected[e.value] === true}
                    disabled={e.original.state=='approved'||e.original.state=='rejected'} onChange={() => this.toggleRow(e,e.value)} id={e.value} value={e.original.schemeID+"~"+e.original.walletRequestId+"~"+e.original.amount+"~"+e.original.paymentMode+"~"+e.original.referenceNumber} />
                    
            },
            {
                Header: "Status",
                id:"state",
                accessor: "state",
                //maxWidth: "110",
                //width: "215",
                headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
              
                //filterMethod: (filter, row) => row[filter.id].startsWith(filter.value),            
                Cell: e =><span style={{color:e.value=="approved"? "LIMEGREEN":e.value=="rejected"?"ORANGERED":""}} > {e.value==null?e.value:e.value[0].toUpperCase()+e.value.slice(1)} </span>
                    
            },
            /*{
            Header: "Request ID",
            id:"walletRequestId",
            accessor: "walletRequestId",
            maxWidth: "150",
            //width: "215",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            //filterMethod: (filter, row) => row[filter.id].startsWith(filter.value),            
               // Cell: e =><span><a href={e.value} onClick={this.handleOnClick} value = {e.value}> {e.value} </a> <button value = {e.value} onClick={this.handleTree}>View tree</button></span>
                
        },
        {
            Header: "Name",
            id:"Name",
            //width: "350",
            //accessor: d => this.getMemberName(d.schemeID),//"schemeUserDetails.firstName",            
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            //filterMethod: (filter, row) => row[filter.id].startsWith(filter.value),
            Cell: e => <div style={{ textAlign: "left" }} value={Promise.resolve(this.getMemberName(e.original.schemeID))}></div>
            
        },*/
        {
            Header: "Scheme ID",
            accessor: "schemeID",
            id:"schemeID",
            //maxWidth: "100",
            //width: "175",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold" },
            Cell: e =><span><a href={e.value} onClick={this.handleOnClick} value = {e.value}> {e.value} </a> </span>
           
        },
        {
            Header: "Amount",
            accessor: "amount",
            id:"amount",
            //maxWidth: "200",
            //width: "175",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            
        }
        ,
        {
            Header: "Requested Date",
            accessor: "requestedDate",
            id:"date",
            //maxWidth: "120",
            //width: "175",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            Cell: e =><span> {e.value==null?"":(new Date(e.value)).getDate() + "/"+ ((new Date(e.value)).getMonth()+ 1)+"/"+(new Date(e.value)).getFullYear()} </span>
            
            
        },
        {
            Header: "Payment Mode",
            accessor: "paymentMode",
            id:"paymentMode",
            //maxWidth: "150",
            //width: "175",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            Cell: e =><span> {e.value==null?e.value:e.value[0].toUpperCase()+e.value.slice(1)} </span>
            
        },
        {
            Header: "Reference Number",
            accessor: "referenceNumber",
            id:"referenceNumber",
            //maxWidth: "180",
            //width: "175",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            Cell: e =><span> {e.value==null?"":e.value[0]+e.value.slice(1)} </span>
            
        },
        {
            Header: "Comments",
            accessor: "comments",
            id:"comments",
            Width:"350",
            //width: "175",
            headerStyle: {textAlign:'center',backgroundColor: 'powderblue',fontWeight: "bold"},
            Cell: e =><span> {e.value==null?"":e.value[0].toUpperCase()+e.value.slice(1)} </span>
            
        }
    ]
    const Modal = () => (
      
        <Popup
        style={{width:'auto',overflow: 'auto'}}
        min-size="80%"
        open={this.state.open}
          modal
          closeOnDocumentClick
          onClose={this.closeModal}       
        >
          <div className="schemeContent">
                  <div>
                <label>FIRST NAME<span style={{color:"red", alignSelf:"center"}}>*</span></label></div>
              <div>
                <input styple={{ alignSelf:"center"}} type="text" name="firstName" placeholder="First Name" defaultValue={this.state.userDetail.schemeUserDetails.firstName} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>LAST NAME</label></div>
              <div> <input type="text" name="lastName" placeholder="Last Name" defaultValue={this.state.userDetail.schemeUserDetails.lastName} onChange={this.changeHandler} onBlur={this.blurHandler}/> 
              </div>
              <div>
                <label>MOBILE NUMBER<span style={{color:"red"}}>*</span></label></div>
              <div> <input type="text" name="phoneNumber" placeholder="Phone Number" defaultValue={this.state.userDetail.schemeUserDetails.mobile} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>GENDER<span style={{color:"red"}}>*</span></label></div>
              <div>
                <input type="radio" name="gender" onChange={this.onRadioChange} defaultChecked={this.state.userDetail.schemeUserDetails.gender === 'male'} value="male" /> Male
              <input type="radio" name="gender" onChange={this.onRadioChange}  defaultChecked={this.state.userDetail.schemeUserDetails.gender === 'female'} value="female" /> Female
    
              </div>
              <div>
                <label>DATE OF BIRTH</label></div>
              <div> <input type="text" name="dob" placeholder="Date of Birth" defaultValue={this.state.userDetail.schemeUserDetails.dateOfBirth} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>ADDRESS 1<span style={{color:"red"}}>*</span></label></div>
              <div> <input type="text" name="address1" placeholder="Address1" defaultValue={this.state.userDetail.userAddressDetails.address1} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>ADDRESS 2</label></div>
              <div> <input type="text" name="address2" placeholder="Address2" defaultValue={this.state.userDetail.userAddressDetails.address2} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>CITY</label></div>
              <div> <input type="text" name="city" placeholder="City" defaultValue={this.state.userDetail.userAddressDetails.city} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>PINCODE</label></div>
              <div> <input type="text" name="pincode" placeholder="Pincode" defaultValue={this.state.userDetail.userAddressDetails.city} onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>EMAIL ID</label></div>
              <div>  <input type="text" name="email" defaultValue={this.state.userDetail.schemeUserDetails.email} placeholder="Email" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>PAN CARD</label></div>
              <div> <input type="text" name="pancard" defaultValue={this.state.userDetail.userBankDetails.pan} placeholder="Pan Card" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>BANK NAME</label></div>
              <div> <input type="text" name="bankName" defaultValue={this.state.userDetail.userBankDetails.bankName} placeholder="Bank Name" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>BRANCH</label></div>
              <div> <input type="text" name="branch" defaultValue={this.state.userDetail.userBankDetails.branch} placeholder="Bank branch" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>ACCOUNT NUMBER</label></div>
              <div> <input type="text" name="accountNo" defaultValue={this.state.userDetail.userBankDetails.accountNumber} placeholder="Bank Account Number" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>IFSC CODE</label></div>
              <div> <input type="text" name="ifscCode" defaultValue={this.state.userDetail.userBankDetails.ifscCode} placeholder="Bank IFSC Code" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              <div>
                <label>SPONSER ID</label></div>
              <div> <input type="text" name="spnserId" disabled={true} defaultValue={this.state.userDetail.referrerUserId} placeholder="Bank IFSC Code" onChange={this.changeHandler} onBlur={this.blurHandler}/>
              </div>
              
              </div>
        </Popup>
      );
    
        //const  { schemeId, name, balance, paid } = this.state;
        return(
          <LoadingOverlay
  active={this.state.overlayVisible}
  spinner
  text='Processing...'
  >
            <div id="topupRequest">
                <h1 style={{ color: 'maroon' }}>Approve/Reject Wallet Topup Request</h1>
                <Modal/>
                <ReactTable
                 getTrProps = {(state, rowInfo, instance) => {
                    if (rowInfo) {
                      return {
                        onClick: (e) => {
                        console.log(document.getElementById(rowInfo.row.checkbox).checked);
                          rowInfo.backgroundColor= document.getElementById(rowInfo.row.checkbox).checked? 'green':'red';
                          }
                        
                        
                      }
                    }
                    return {};
                  }}
                style={{
                    //width: '100%',
                    backgroundColor: 'HONEYDEW' ,
                    display: 'flex',
                    justifyContent:'center',
                    align:'middle'                   
                  }}
                    filterable
                    defaultFiltered={[
                      {
                        id: 'state',
                        value: 'Pending'
                      }
                    ]}
                    
          defaultFilterMethod={(filter, row) =>
            (String(row[filter.id])).toLowerCase().startsWith((filter.value).toLowerCase())}
           
          
                    columns={columns}
                    data ={this.state.requestDetails}
                    className="-striped -highlight"
                    defaultPageSize={8}
                >

                </ReactTable>
                <div>
                <button className="schemePageButton" style={{fontSize:"18px", width:"110px", height:"40px", fontWeight:"600"}} onClick = {this.approveHandler} >Approve</button>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <button className="schemePageButton" style={{fontSize:"18px", width:"110px", height:"40px", fontWeight:"600"}} onClick = {this.rejectHandler} >Reject</button>
                </div>
                {this.state.alert}
                </div>
                </LoadingOverlay>
        ); }
}
export default WalletRequest;