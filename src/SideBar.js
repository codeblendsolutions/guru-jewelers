import React, { Component } from 'react';
import { scaleRotate as Menu } from "react-burger-menu";
import { Link } from 'react-router-dom';
import './Hamburger.css';
class sidebar extends Component{
  constructor(props) {
    super(props)
    this.state = {
      menuOpen:false
    }
    this.handleStateChange=this.handleStateChange.bind(this);
  this.handleLinkClick=this.handleLinkClick.bind(this); 
  }

  handleStateChange(state) {
    this.setState({menuOpen:state.isOpen})
  }

  handleLinkClick(e) {
    this.setState({menuOpen:false})
  }
  render(){
  return (
    // Pass on our props
    <Menu onStateChange={this.handleStateChange} isOpen={this.state.menuOpen}>
      <nav className="hamBurger-navigation">
        <ul className="bm-item hamBurger-nav-links">
          <img src={process.env.PUBLIC_URL + "/images/coin.png"} className="sgjSideBarCoin" alt="coin" />
          <h3 className="trustClass">SGJ</h3>

          {localStorage.getItem("customerType") === 'admin' ?
            <span>ID - {localStorage.getItem('schemeId')}</span>

            :
            <span>ID - {localStorage.getItem('schemeId')}</span>

          }
          <div>&nbsp;</div>
          {localStorage.getItem("customerType") === 'admin' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/schemeForm' onClick={()=>this.handleLinkClick()}>
              <li>
                Scheme Form
             </li>
            </Link> : null}
			 {localStorage.getItem("customerType") === 'customer' && localStorage.getItem("walletTopUpAmount") >= 300 ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/schemeForm' onClick={()=>this.handleLinkClick()}>
              <li>
                Scheme Form
             </li>
            </Link> : null}
			
		{localStorage.getItem("customerType") === 'admin' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/oldTree' onClick={()=>this.handleLinkClick()}>
            <li>
              Old Stage Tree
             </li>
          </Link>  : null}
			
		  {localStorage.getItem("customerType") === 'admin' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/completeReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
              All Stage Tree
             </li>
          </Link>  : null}
		  
		 
		  {localStorage.getItem("customerType") === 'customer' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage1ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             LEVEL 1 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level1Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage2ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             LEVEL 2 Tree
             </li>
          </Link>  : null}
		   {localStorage.getItem("level2Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage3ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             LEVEL 3 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level3Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage4ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             LEVEL 4 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level4Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage5ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             LEVEL 5 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level5Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage6ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             lEVEL 6 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level6Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage7ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             Stage 7 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level7Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage8ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             Stage 8 Tree
             </li>
          </Link>  : null}
		  {localStorage.getItem("level8Completed") === 'true' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/stage9ReferralTree' onClick={()=>this.handleLinkClick()}>
            <li>
             Stage 9 Tree
             </li>
          </Link>  : null}
		  	{localStorage.getItem("customerType") === 'customer' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/wallet' onClick={()=>this.handleLinkClick()}>
            <li>
              Wallet Details
             </li>
           </Link> : null}
		   {localStorage.getItem("customerType") === 'customer' ?
          <Link className="bm-item hamBurger-link-style" to='/sgj/walletPassBook' onClick={()=>this.handleLinkClick()}>
            <li>
             Wallet PassBook
             </li>
          </Link>  : null}
		   {localStorage.getItem("customerType") === 'admin' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/stageWalletPay' onClick={()=>this.handleLinkClick()}>
              <li>
                Scheme Wallet Pay
             </li>
            </Link> : null}
			{localStorage.getItem("customerType") === 'admin' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/stageWalletPaidStatus' onClick={()=>this.handleLinkClick()}>
              <li>
                Wallet Paid Status
             </li>
            </Link> : null}
			{localStorage.getItem("customerType") === 'admin' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/stageReturnOfInvestment' onClick={()=>this.handleLinkClick()}>
              <li>
                Return of Investment
             </li>
            </Link> : null}
			{localStorage.getItem("customerType") === 'admin' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/memberSearch' onClick={()=>this.handleLinkClick()}>
              <li>
                Member Search
             </li>
            </Link> : null}
			{localStorage.getItem("customerType") === 'admin' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/walletRequest' onClick={()=>this.handleLinkClick()}>
              <li>
                Wallet Request Details
             </li>
            </Link> : null}
			{localStorage.getItem("customerType") === 'customer' ?
            <Link className="bm-item hamBurger-link-style" to='/sgj/walletTopUpRequest' onClick={()=>this.handleLinkClick()}>
              <li>
                Wallet TopUp Request
             </li>
            </Link> : null}
          <Link className="bm-item hamBurger-link-style" to='/sgj/changePassword' onClick={()=>this.handleLinkClick()}>
            <li>
              Change Password
             </li>
          </Link>
		  
        </ul>
      </nav>
    </Menu>
  );
        }
}
export default sidebar;
