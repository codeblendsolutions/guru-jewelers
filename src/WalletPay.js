import React, { PureComponent } from 'react';
import Axios from 'axios';
import "react-table/react-table.css";
import SweetAlert from 'react-bootstrap-sweetalert';
import "./App.css";
import * as myConstClass from './Constants.js';
import LoadingOverlay from 'react-loading-overlay';

//import matchSorter from 'match-sorter'

class WalletPay extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      mobile: '',
      schemeId: '',
      balance: 0,
      paid: 0,
      userDetail: {
        schemeUserDetails: "",
        userAddressDetails: "",
        userBankDetails: "",
        schemeWalletDetails: ""
      },
      validation: '',
      alert: null,
      overlayVisible: false
    }
  }
  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }

  blurHandler = async (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if (name === "schemeId") {
      var url = myConstClass.SCHEME_FORM_URL + '/' + String(value).toUpperCase();
      console.log("url : " + url);
      await Axios.get(url)
        .then((response) => {
          this.setState({
            userDetail: response.data,
            name: response.data.schemeUserDetails.firstName[0].toUpperCase() + response.data.schemeUserDetails.firstName.slice(1) + " " + response.data.schemeUserDetails.lastName[0].toUpperCase() + response.data.schemeUserDetails.lastName.slice(1),
            mobile: response.data.schemeUserDetails.mobile

          })
            response.data.schemeWalletDetails != null? this.setState({balance:response.data.schemeWalletDetails.availableLevel1Balance}) : this.setState({balance:0})
        })
        .catch(error => {
          console.log(error);
          this.setState({
            name: "",
            balance: 0
          }
          )
        });

    }
  }
  submitHandler = async (e) => {
    if ((parseInt(this.state.balance) > 0) && (parseInt(this.state.paid) > 0)) {
      this.setState({ overlayVisible: true })
      const params =
      {
        schemeID: String(this.state.schemeId).toUpperCase(),
        payBalance: this.state.paid,
        modeOfPayment: 'Cash',
        bankRef: '',
      }
      console.log("params : " + params);
      var url = myConstClass.WALLET_PAY;
      // var url = 'http://localhost:8080/schemeDetails';

      Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
        //this.state.validation = response.data;
        this.setState({
          validation: response.data
        })
        console.log("validated : " + this.state.validation.validated);

        console.log("After Axios" + this.state.validation);


        var success = "Success  - " + this.state.validation.message;
        var error = "Error !  " + this.state.validation.message;

        console.log(success);
        console.log(error);
        //console.log("scheme ID : "+this.state.validation.createdSchemeId);


        if (this.state.validation.validated === true) {
          //console.log("WElcome validated");
          //const MSG91 = require('msg91');
          this.msg91 = require("msg91")("292308AgXOs1Sm5d6dfe1c", "SGJWLR", 4);
          var message = "Greetings " + this.state.name + "! Payment of amount Rs." + this.state.paid + "/- has been made. Regards, Sri Guru Jewellers";
          this.msg91.send(this.state.mobile, message, function (err, response) {
            console.log(err);
            console.log(response);
          });



          const getAlert = () => (
            <SweetAlert
              success
              title="Payment Updated"
              onConfirm={() => this.hideAlert()}
              value={success}
            >
            </SweetAlert>
          );
          this.setState({ overlayVisible: false })
          this.setState({
            alert: getAlert()
          });
          document.getElementById('schemeId').focus();
          document.getElementById('paid').focus();
          this.state.paid = "";

        }
        else {
          console.log("sorry not  validated" + error);
          /*  this.setState({
             showErrorMessage: true,
             successMessage : this.state.validation.message
 
           }) */

          const getAlert = () => (
            <SweetAlert
              error
              title={error}
              onConfirm={() => this.hideAlert()}
              value={error}
            >

            </SweetAlert>
          );
          this.setState({ overlayVisible: false })
          this.setState({
            alert: getAlert()
          });

        }
      }).catch(error => {
        console.log(error);
        const getAlert = () => (
          <SweetAlert
            error
            title="Update failed!"
            onConfirm={() => this.hideAlert()}
            value={error}
          >

          </SweetAlert>
        );
        this.setState({ overlayVisible: false })
        this.setState({
          alert: getAlert()
        });

      });
    }
    else {
      const getAlert = () => (
        <SweetAlert
          error
          title="Validate the balance and the amount paid!"
          onConfirm={() => this.hideAlert()}
          value=""
        >

        </SweetAlert>
      );
      this.setState({
        alert: getAlert()
      });
    }
  }


  render() {
    const { schemeId, name, balance, paid } = this.state;
    return (
      <LoadingOverlay
        active={this.state.overlayVisible}
        spinner
        text='Processing...'
      >
        <div id="walletPay">
          <h1 style={{ color: 'maroon' }}>Pay From Wallet</h1>
          <div className="block">
            <label style={{ display: "inline-block", width: "170px", textAlign: "left", fontSize: "20px" }}>Scheme ID &nbsp; &nbsp;</label>
            <input style={{ fontSize: "20px" }} id="schemeId" type="text" name="schemeId" placeholder="" value={schemeId} onChange={this.changeHandler} onBlur={this.blurHandler} />
          </div>
          <div>&nbsp;</div>
          <div className="block">
            <label style={{ display: "inline-block", width: "170px", textAlign: "left", fontSize: "20px" }}>Member Name &nbsp; &nbsp;</label>
            <input style={{ fontSize: "20px" }} disabled={true} type="text" name="name" placeholder="" value={name} onChange={this.changeHandler} />
          </div>
          <div>&nbsp;</div>
          <div className="block">
            <label style={{ display: "inline-block", width: "170px", textAlign: "left", fontSize: "20px" }}>Wallet Balance &nbsp; &nbsp;</label>
            <input style={{ fontSize: "20px" }} disabled={true} type="text" name="balance" placeholder="" value={balance} onChange={this.changeHandler} />
          </div>
          <div>&nbsp;</div>
          <div className="block">
            <label style={{ display: "inline-block", width: "170px", textAlign: "left", fontSize: "20px" }}>Amount paid &nbsp; &nbsp;</label>
            <input style={{ fontSize: "20px" }} id="paid" type="text" name="paid" placeholder="" value={paid} onChange={this.changeHandler} />
          </div>
          <div>&nbsp;</div>
          <div>
            <button className="schemePageButton" style={{ fontSize: "22px", width: "120px", height: "50px", fontWeight: "600" }} onClick={this.submitHandler} >Submit</button>
          </div>
          {this.state.alert}
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
          <div>&nbsp;</div>
        </div>
      </LoadingOverlay>


    )
  }
}
export default WalletPay;