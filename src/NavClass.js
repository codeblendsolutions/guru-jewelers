import React from 'react';
import { Link } from "react-router-dom";


class NavClass extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            toText: 'Login',
            toLink: '/login',
            isLoggedIn: false,
            logOutText: 'LogOut'
        }
        this.handleLoginScenario = this.handleLoginScenario.bind(this);
    }

    componentDidMount() {
        //  console.log("comppo moint + " + localStorage.getItem('isLoggedIn'));
        // console.log("comppo "+ this.props.anyString);
        //   console.log("ml check logout " + this.state.toText == this.state.logOutText);
        // console.log("ml check Login " + this.state.toText == this.state.toText);
        if (localStorage.getItem('isLoggedIn') === 'true') {

            this.setState({
                toText: 'LogOut',
                toLink: '/home',
                isLoggedIn: true
            })
            localStorage.setItem('isLoggedInText', 'LogOut');
        } else {
            this.setState({
                toText: 'Login',
                toLink: '/login',
                isLoggedIn: false
            })
            localStorage.setItem('isLoggedInText', 'Login');
        }

        // window.location.reload();

    }

    componentDidUpdate(prevprops, prevstate) {
        //   console.log("Check when this is called");
        //  console.log("asjhadkj "+ this.props.anyString);
    }

    handleLoginScenario = (e) => {

        console.log(localStorage.getItem('isLoggedIn'));


        if (localStorage.getItem('isLoggedIn') === 'true' && localStorage.getItem('isLoggedInText') === 'LogOut') {
            console.log("logged in and text is log out");
            localStorage.setItem('isLoggedIn', false);
            this.setState({
                toText: 'Login',
                toLink: '/login',
                isLoggedIn: false
            })
        }


    }

    render() {
        return (


            <nav className="Navigation">
                
                <div style={{display: "flex"}}>
                <img src={process.env.PUBLIC_URL + '/images/coin.png'} className="sgjCoin" alt="coin" />
                &nbsp;
                &nbsp;
                &nbsp;
                <h3>SRI GURU JEWELLERY</h3>
                </div>
                <ul className="nav-links">
                    <Link className="link-style" to='/home'>
                        <li>
                            Home
                        </li>
                    </Link>
                    <Link className="link-style" to='/about'>
                        <li>
                            About
                        </li>
                    </Link>

                    <Link onClick={this.handleLoginScenario} className="link-style" to={this.state.toLink}>
                        <li>
                            {this.state.toText}
                        </li>
                    </Link>
                </ul>
            </nav>
        )
    }
}

export default NavClass;