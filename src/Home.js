import React from 'react';
import './App.css';
import { Gallery, GalleryImage } from 'react-gesture-gallery';
import { Link } from 'react-router-dom';



function Home() {

  const [index, setIndex] = React.useState(0);
  const isLoggedIn = localStorage.getItem('isLoggedIn') === 'true';


  let toVal = "/login";

  if (isLoggedIn) {
    if (localStorage.getItem("customerType") === 'admin')
      toVal = '/sgj/schemeForm';
    else {
      toVal = '/sgj/stage1ReferralTree';
    }
  } else {
    toVal = '/login';


  }

  const images = [
    "/images/welcome2.jpg",
    "/images/QuoteBanner.jpeg",
	"/images/EasyCashLoan.jpeg",
	"/images/CashEarningPlan.jpeg",
	"/images/GoldEarningPlan.jpeg",
	"/images/DocumentDetails.jpeg",
	"/images/Thannimbikai.jpeg",


  ];
  React.useEffect(() => {

    const interval = setInterval(() => {

      if (index === images.length - 1) {
        setIndex(0)
      } else {
        setIndex(index + 1)
      }

    }, 4500);

    return () => clearInterval(interval)
  }, [index])


  return (

    <Gallery
      style={{
        width: '100%',
        height: '100%'
      }}
      index={index}
      onRequestChange={i => {
        setIndex(i);
      }}
    >
      {images.map(

        image =>
          <Link className="link-style" key={image} to={toVal}>
            <GalleryImage objectFit="contain" key={image} src={image} />
          </Link>
      )}
    </Gallery>
  );
}

export default Home;
