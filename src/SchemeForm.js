import React, { Component } from 'react';
import './App.css';
import Axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import SmoothScrolling from "./SmoothScrolling";
import * as myConstClass from './Constants.js';
import LoadingOverlay from 'react-loading-overlay';


class SchemeForm extends Component {

  mobileNo = "";
  msg91;
  constructor(props) {
    super(props)
    this.state = {
      packageAmount: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      gender: '',
	  adminAccess: '',
      dob: '',
      date1: null,
      address1: '',
      address2: '',
      city: '',
      pincode: '',
      email: '',
      pancard: '',
      bankName: '',
      branch: '',
      accountNo: '',
      ifscCode: '',
      reference: '',
      userId: '',
      modeOfPayment: '',
      showUserId: false,
      showSuccessMessage: false,
      successMessage: '',
      errorMessage: '',
      showErrorMessage: false,
      formErrors: { email: '', phoneNumber: '', firstName: '', userId: '', dob: '' },
      emailValid: false,
      phoneNumberValid: false,
      firstNameValid: false,
      userIdValid: true,
      dobValid: false,
      genderValid: false,
      refferenceValid: false,
      address1Valid: false,
      alert: null,
      validation: [],
      formValid: false,
      loggedInSchemeId: localStorage.getItem('schemeId'),
      overlayVisible: false
    }
    this.onRadioChange = this.onRadioChange.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

componentDidMount() {
		
		 this.setState({
      adminAccess: 'NO'
    })
         
		
    }


  onRadioChange(e) {

    // console.log(e.target.name  +" - " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    })
    /*if (e.target.value === "sponsor") {
      // console.log("plese enter sponsor id");
      //console.log("sponsor is clicked");
      this.setState({
        reference: "sponsor",
        showUserId: true,
        //   userIdValid: false
      })
    } else if (e.target.value === "admin") {
      //     console.log("admin is clicked");

      //  var schemeID = localStorage.getItem('schemeId');
      this.setState({
        reference: "admin",
        showUserId: false,
        //   userIdValid:true

      }, () => {
        //      console.log("function reference : "+this.state.reference);        
      })
      //console.log("reference : "+this.state.reference);
    }*/
  }


  hideAlert() {
    console.log('Hiding alert...');
    this.setState({
      alert: null
    });
  }
  sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds) {
        break;
      }
    }
  }
  submitHandler = (e) => {
    e.preventDefault();
    var fValid = false;
    SmoothScrolling.scrollTo("begin");
    console.log("before overlay");
    this.setState({ overlayVisible: true })
    console.log("after overlay");
    console.log("formValid : " + this.state.formValid);

    const numberMatch = /^([0-9]?\d{10})$/i;
    const dobMatch = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/i;

 console.log("admin Access : "+ this.state.adminAccess);
 
    console.log("gender Value : " + this.state.gender);
    // console.log("gender checked : "+this.state.gender.checked);
    console.log("reference value : " + this.state.reference);

    console.log("firstName : " + this.state.firstName === '');

    console.log("phoneNumber : " + this.state.phoneNumber === "");


    if (this.state.firstName === "" && this.state.gender === "" && this.state.phoneNumber === "" && this.state.address1 === "" && this.state.reference === "") {
      this.setState({
        showErrorMessage: true,
        formValid: false,
        errorMessage: 'Please enter all the required fields'
      })
      fValid = false;

    } else if (!numberMatch.test(this.state.phoneNumber)) {
      this.setState({
        showErrorMessage: true,
        formValid: false,
        errorMessage: 'Please enter Valid Phone Number'
      })
      fValid = false;
    } else if ((this.state.dob !== "") && (!dobMatch.test(this.state.dob))) {
      this.setState({
        showErrorMessage: true,
        formValid: false,
        errorMessage: 'Please enter Date of birth in : DD/MM/YYYY'
      })
      fValid = false;
    } else if (!(this.state.gender === 'male' || this.state.gender === 'female')) {
      this.setState({
        showErrorMessage: true,
        formValid: false,
        errorMessage: 'Please select valid gender type'
      })
      fValid = false;
    } else if (this.state.address1 === "") {

      this.setState({
        showErrorMessage: true,
        formValid: false,
        errorMessage: 'Please enter valid address'
      })
      fValid = false;
    } else if (this.state.userId === "") {
      this.setState({
        showErrorMessage: true,
        formValid: false,
        errorMessage: 'Please Enter a Sponsor ID'
      })
      fValid = false;
    } else {

      this.setState({
        showErrorMessage: false,
        formValid: true,
        errorMessage: ''
      })
      fValid = true;
    }

    if (fValid) {
     /* if (this.state.reference === "admin") {

        this.state.userId = localStorage.getItem('schemeId');
        this.state.reference = localStorage.getItem('customerType') === 'customer' ? 'sponsor' : 'admin';


      }*/
	  
	  
	 console.log("Reference User ID  : " + this.state.userId );
      /*for(var i=0;i<10000;i++)
      {console.log("hi");}*/
      const params =

      /*  {
          packageAmount: '1000',
          reference: this.state.reference,
          referrerUserId: this.state.userId,
          schemeUserType: 'customer',
          loggedInSchemeID: localStorage.getItem('schemeId'),
          schemeUserDetails: {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            gender: this.state.gender,
            mobile: this.state.phoneNumber,
            dateOfBirth: this.state.dob,
            email: this.state.email
          },
          userAddressDetails: {
            address1: this.state.address1,
            address2: this.state.address2,
            city: this.state.city,
            state: this.state.state,
            pincode: this.state.pincode
          },
          userBankDetails: {
            bankName: this.state.bankName,
            accountNumber: this.state.accountNo,
            branch: this.state.branch,
            ifscCode: this.state.ifscCode,
            pan: this.state.pancard,
            modeOfPayment: this.state.modeOfPayment
          }
    
    
        }
    */


      {
        schemePackageDetails: {
          packageAmount: '200',
          reference: 'sponsor',
          referrerUserId: this.state.userId,
          schemeUserType: 'customer',
		  adminAccess : this.state.adminAccess,
          loggedInSchemeID: localStorage.getItem('schemeId'),
		  walletTopUpAmount: localStorage.getItem('walletTopUpAmount')
        },
        schemeUserDetails: {
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          gender: this.state.gender,
          mobile: this.state.phoneNumber,
          dateOfBirth: this.state.dob,
          email: this.state.email
        },
        userAddressDetails: {
          address1: this.state.address1,
          address2: this.state.address2,
          city: this.state.city,
          state: this.state.state,
          pincode: this.state.pincode
        },
        userBankDetails: {
          bankName: this.state.bankName,
          accountNumber: this.state.accountNo,
          branch: this.state.branch,
          ifscCode: this.state.ifscCode,
          pan: this.state.pancard,
          modeOfPayment: this.state.modeOfPayment
        }
      }
      console.log("params : " + params);


      const url = myConstClass.SCHEME_FORM_URL;
      console.log("url : " + url);

      Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
        //this.state.validation = response.data;
        this.setState({
          validation: response.data
        })
        console.log("validated : " + this.state.validation.validated);

        console.log("After Axios");


        var success = "Success  - " + this.state.validation.message;
        var error = "Error !  " + this.state.validation.message;

        console.log(success);
        console.log(error);
        console.log("scheme ID : " + this.state.validation.createdSchemeId);


        if (this.state.validation.validated === true) {
          console.log("WElcome validated");
		    console.log("this.state.validation.walletTopUpAmount : " + this.state.validation.walletTopUpAmount);
			localStorage.setItem('walletTopUpAmount', this.state.validation.walletTopUpAmount);


             const MSG91 = require('msg91');
             this.msg91 = require("msg91")("292308AgXOs1Sm5d6dfe1c", "SGJWLR", 4 );
             this.mobileNo = this.state.phoneNumber;
             this.msg91.getBalance(4, function(err, msgCount){
                 console.log(err);
                 console.log(msgCount);
             });
     
             var message = "Welcome to Sri Guru Jewellers. Dear customer, Scheme has been succesfully registered with SCHEME ID : "+this.state.validation.createdSchemeId +". and Password : "+this.state.validation.createdSchemeId+". Thank you for payment of Rs - 300. Please visit www.sri-gurujewellery.com for more details.";
             this.msg91.send(this.mobileNo, message , function(err, response){
                 console.log(err);
                 console.log(response);
             });
             

          const getAlert = () => (
            <SweetAlert
              success
              title={success}
              onConfirm={() => this.hideAlert()}
              value={success}
            >
            </SweetAlert>
          );
          this.setState({
            overlayVisible: false,
            firstName: "",
            lastName: "",
            phoneNumber: "",
            userId: "",
            address1: "",
            dob: "",
            reference: 'admin',
            showUserId: false

          })
          this.setState({
            alert: getAlert()

          });

        }
        else {
          console.log("sorry not  validated" + error);
          /*  this.setState({
             showErrorMessage: true,
             successMessage : this.state.validation.message
 
           }) */

          const getAlert = () => (
            <SweetAlert
              error
              title={error}
              onConfirm={() => this.hideAlert()}
              value={error}
            >

            </SweetAlert>
          );
          this.setState({
            overlayVisible: false,
            firstName: "",
            lastName: "",
            phoneNumber: "",
            userId: "",
            address1: "",
            dob: ""


          })
          this.setState({
            alert: getAlert()
          });

        }
      }).catch(error => {
        console.log(error.message);
        const getAlert = () => (
          <SweetAlert
            error
            title={error.message}
            onConfirm={() => this.hideAlert()}
            value={error.message}
          >

          </SweetAlert>
        );
        this.setState({ overlayVisible: false })
        this.setState({
          alert: getAlert()
        });
      });

    } else {
      this.state.formErrors = "Please enter required fields";
      this.setState({ overlayVisible: false })
    }
  }
  render() {
    const { packageAmount, firstName, lastName, phoneNumber, gender, dob, address1, address2, city, pincode, email, pancard, bankName, branch, accountNo, ifscCode, reference, userId, modeOfPayment, alert } = this.state;



    return (
      <LoadingOverlay
        active={this.state.overlayVisible}
        spinner
        text='Submitting your form...'
      >

        <div id="begin">

          <h1 style={{ color: 'maroon' }}> Scheme Registration</h1>
          <div className="panel panel-default">
            <div style={{ textAlign: 'right', color: 'red' }} className="text-right"><h3> * fields are mandatory</h3> </div>
          </div>
          {this.state.showErrorMessage ?
            <div><h1 style={{
              color: 'red'
            }}> {this.state.errorMessage}</h1>
            </div>
            : null}
          {this.state.alert}
          <form onSubmit={this.submitHandler}>
            <div className="schemeContent">
              <div>
                <label>PACKAGE</label></div>
              <div>
                <select name="packageAmount">
                  <option value="300">Package 300 /- </option>
                </select>
              </div>
              <div>
                <label>FIRST NAME<span style={{ color: "red" }}>*</span></label></div>
              <div>
                <input type="text" name="firstName" placeholder="First Name" value={firstName} onChange={this.changeHandler} />
              </div>
              <div>
                <label>LAST NAME</label></div>
              <div> <input type="text" name="lastName" placeholder="Last Name" value={lastName} onChange={this.changeHandler} />
              </div>
              <div>
                <label>MOBILE NUMBER<span style={{ color: "red" }}>*</span></label></div>
              <div> <input type="text" name="phoneNumber" placeholder="Phone Number" value={phoneNumber} onChange={this.changeHandler} />
              </div>
              <div>
                <label>GENDER<span style={{ color: "red" }}>*</span></label></div>
              <div>
                <input type="radio" name="gender" onChange={event => this.onRadioChange(event)} value="male" /> Male
              <input type="radio" name="gender" onChange={event => this.onRadioChange(event)} value="female" /> Female

            </div>
              <div>
                <label>DATE OF BIRTH</label></div>
              <div>
                <input type="text" name="dob" placeholder="DD/MM/YYYY" value={dob} onChange={this.changeHandler} />

              </div>
              <div>
                <label>ADDRESS 1<span style={{ color: "red" }}>*</span></label></div>
              <div> <input type="text" name="address1" placeholder="Address1" value={address1} onChange={this.changeHandler} />
              </div>
              <div>
                <label>ADDRESS 2</label></div>
              <div> <input type="text" name="address2" placeholder="Address2" value={address2} onChange={this.changeHandler} />
              </div>
              <div>
                <label>CITY</label></div>
              <div> <input type="text" name="city" placeholder="City" value={city} onChange={this.changeHandler} />
              </div>
              <div>
                <label>PINCODE</label></div>
              <div> <input type="text" name="pincode" placeholder="Pincode" value={pincode} onChange={this.changeHandler} />
              </div>
              <div>
                <label>EMAIL ID</label></div>
              <div>  <input type="text" name="email" value={email} placeholder="Email" onChange={this.changeHandler} />
              </div>
              <div>
                <label>PAN CARD</label></div>
              <div> <input type="text" name="pancard" value={pancard} placeholder="Pan Card" onChange={this.changeHandler} />
              </div>
              <div>
                <label>BANK NAME</label></div>
              <div> <input type="text" name="bankName" value={bankName} placeholder="Bank Name" onChange={this.changeHandler} />
              </div>
              <div>
                <label>BRANCH</label></div>
              <div> <input type="text" name="branch" value={branch} placeholder="Bank branch" onChange={this.changeHandler} />
              </div>
              <div>
                <label>ACCOUNT NUMBER</label></div>
              <div> <input type="text" name="accountNo" value={accountNo} placeholder="Bank Account Number" onChange={this.changeHandler} />
              </div>
              <div>
                <label>IFSC CODE</label></div>
              <div> <input type="text" name="ifscCode" value={ifscCode} placeholder="Bank IFSC Code" onChange={this.changeHandler} />
              </div>
			  
			  
			  {localStorage.getItem("customerType") === 'admin' ?
				<div><label>Admin ID<span style={{ color: "red" }}>*</span></label></div>
				: null}
				
				{localStorage.getItem("customerType") === 'admin' ?
				<div>
					<input type="radio" name="adminAccess" onChange={event => this.onRadioChange(event)} value="YES" /> YES
					<input type="radio" name="adminAccess" onChange={event => this.onRadioChange(event)} value="NO"  defaultChecked /> NO

				</div>
				: null}
			
			
			
                <div>
                  <label>Sponsor User ID<span style={{ color: "red" }}>*</span></label></div>
                <div> <input type="text" name="userId" value={userId} placeholder="Referrer User Id " onChange={this.changeHandler} />
                </div>
                
              <div>
                <label>MODE OF PAYMENT</label></div>
              <div>
                <input type="radio" name="modeOfPayment" onChange={this.onRadioChange} value="cash" /> Cash
            <input type="radio" name="modeOfPayment" onChange={this.onRadioChange} value="others" /> Cheque/DD

            </div>
              <div><button className="schemePageButton" type="submit" >Submit</button></div>
            </div>
            {this.state.showSuccessMessage ?
              <div><h1 style={{
                color: 'green'
              }}> {this.state.successMessage}</h1>
              </div>
              : null}


          </form>
        </div>
      </LoadingOverlay>


    )

  }
}

export default SchemeForm;