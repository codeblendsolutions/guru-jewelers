import React, { Component } from 'react';
import Tree from 'react-d3-tree';
import Axios from 'axios';
import * as myConstClass from './Constants.js';

class OldReferralTree extends Component {

    constructor(props) {
        super(props)


        this.state = {
            refferalData: [
                {
                    "name": localStorage.getItem('schemeId'),
                    "parent": "null",
                    "value": "Welcome"
                }
            ]
        };
    }


    componentDidMount() {


        var schemeID = localStorage.getItem('schemeId');

        var schemeAmount = localStorage.getItem('availableSchemeAmount');

        console.log("availableSchemeAmount : " + parseInt(schemeAmount));
        if ((parseInt(localStorage.getItem('availableSchemeAmount'))) >= 1000)
            console.log("success mama");
        const url = myConstClass.OLD_REFERRAL_TREE_VIEW;
        //const url =  myConstClass.REFERRAL_TREE_VIEW+"";
        console.log("url : " + url);
        Axios.get(url)
            .then(referalList => {
                console.log("referalList : ", referalList);
                console.log("referalList data : ", referalList.data);
                this.setState({
                    refferalData: referalList.data
                })

            })


            .catch(error => {
                console.log(error);
            });
    }


    render() {

        console.log("refferalData : " + this.state.refferalData);

        var data = this.state.refferalData;

        console.log("orgData : " + data);

        var dataMap = data.reduce(function (map, node) {
            map[node.name] = node;
            return map;
        }, {});

        var myTreeData = [];
        data.forEach(function (node) {
            // add to parent
            var parent = dataMap[node.parent];
            if (parent) {
                // create child array if it doesn't exist
                (parent.children || (parent.children = []))
                    // add node to child array
                    .push(node);
            } else {
                // parent is null or missing
                myTreeData.push(node);
            }
        });
        const nodeSvgShape = { shape: 'image', shapeProps: { href: "/images/coin.png", width: 40, height: 40, x: -20, y: - 35, } };
        const linkSvgShape = { stroke: 'green', strokeWidth: 3 };
        return (

            <div id="treeWrapper" style={{ width: '100%', height: '50em' }}>

                <Tree data={myTreeData} nodeSvgShape={nodeSvgShape} orientation={"vertical"} translate={{ x: 700, y: 50 }}
                    allowForeignObjects
                    nodeLabelComponent={{
                        render: <NodeLabel className='myLabelComponentInSvg' />,
                        foreignObjectWrapper: {
                            x: -5,
                            y: -24
                        }
                    }}
                    pathFunc="elbow" styles={{ links: linkSvgShape }} textLayout={{ textAnchor: "start", x: 18, y: -20, transform: undefined }} />

            </div>
        );

    }


}


export default OldReferralTree;

class NodeLabel extends React.PureComponent {
    render() {
        const { className, nodeData } = this.props
        return (
            <div className={className}>
                <span><b>{nodeData.name}</b></span>
                <span><br /><b>{nodeData.value}</b></span>

            </div>
        )
    }
}