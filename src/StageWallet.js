import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';
import 'react-accessible-accordion/dist/fancy-example.css';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
class Wallet extends Component {

    constructor(props) {
        super(props)
        this.state = {
            availableBalance: '',
            stage1CustomerWallet: [],
			stage2CustomerWallet: [],
			stage3CustomerWallet: [],
			stage4CustomerWallet: [],
			stage5CustomerWallet: [],
			stage6CustomerWallet: [],
			stage7CustomerWallet: [],
			stage8CustomerWallet: [],
			stage9CustomerWallet: []
        }

    }

    componentDidMount() {
        var schemeID = localStorage.getItem('schemeId');
        const url = myConstClass.STAGE1_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {
                this.setState.availableBalance = response.data.availableWalletBalance;          
                if (response.data.stage1RefferalDetails != null) {
                    this.setState({
                        availableBalance: response.data.availableWalletBalance,
                        stage1CustomerWallet: response.data.stage1RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			
			if(localStorage.getItem("level1Completed") === 'true'){
				 const url = myConstClass.STAGE2_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage2RefferalDetails != null) {
                    this.setState({
                        stage2CustomerWallet: response.data.stage2RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level2Completed") === 'true'){
				 const url = myConstClass.STAGE3_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage3RefferalDetails != null) {
                    this.setState({
                        stage3CustomerWallet: response.data.stage3RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level3Completed") === 'true'){
				 const url = myConstClass.STAGE4_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage4RefferalDetails != null) {
                    this.setState({
                        stage4CustomerWallet: response.data.stage4RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level4Completed") === 'true'){
				 const url = myConstClass.STAGE5_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage5RefferalDetails != null) {
                    this.setState({
                        stage5CustomerWallet: response.data.stage5RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level5Completed") === 'true'){
				 const url = myConstClass.STAGE6_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage6RefferalDetails != null) {
                    this.setState({
                        stage6CustomerWallet: response.data.stage6RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level6Completed") === 'true'){
				 const url = myConstClass.STAGE7_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage7RefferalDetails != null) {
                    this.setState({
                        stage7CustomerWallet: response.data.stage7RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level7Completed") === 'true'){
				 const url = myConstClass.STAGE8_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage8RefferalDetails != null) {
                    this.setState({
                        stage8CustomerWallet: response.data.stage8RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
			
			if(localStorage.getItem("level8Completed") === 'true'){
				 const url = myConstClass.STAGE9_WALLET_DETAILS + schemeID;
        Axios.get(url)
            .then((response) => {     
                if (response.data.stage9RefferalDetails != null) {
                    this.setState({
                        stage9CustomerWallet: response.data.stage9RefferalDetails
                    })
                }
            })

            .catch(error => {
                console.log(error);
            });
			}
    }


    render() {
		
        const columns = [
            {
                Header: "Referral ID",
                accessor: "referralId"
            },
            {
                Header: "Referral Name",
                accessor: "refferalName"
            },
            {
                Header: "Referral Date",
                accessor: "referralDate"
            }
            ,
			 {
                Header: "Referral Amount",
                accessor: "referralAmount"
            },
            {
                Header: "Referral Level",
                accessor: "referralLevel"
            }
        ]
        return (
            <div id="wallet">
                <h1>Wallet Details</h1>
				<h3>Scheme ID : {localStorage.getItem('schemeId')}</h3>
				
                <h3>Available Balance : {this.state.availableBalance}</h3>


                
				
				
				<Accordion allowZeroExpanded>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton 
					style={{
						color:'white',
						fontWeight: 'bold', 
						backgroundColor: '#800080',
						}}>
                       LEVEL 1
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <ReactTable
                    columns={columns}
                    data={this.state.stage1CustomerWallet}
					pageSize={this.state.stage1CustomerWallet.length}
					showPagination={false}
					
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
			
		{localStorage.getItem("level1Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton
					style={{
						color:'white',
						fontWeight: 'bold',
						backgroundColor: '#FFC0CB',
						}}
					>
                        LEVEL 2
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage2CustomerWallet}
					pageSize={this.state.stage2CustomerWallet.length}
					showPagination={false}
					
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}
	{localStorage.getItem("level2Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton
					style={{
						color:'white',
						fontWeight: 'bold',
						backgroundColor: '#000080',
						}}
					>
                        LEVEL 3
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage3CustomerWallet}
					pageSize={this.state.stage3CustomerWallet.length}
					showPagination={false}
					
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}
				 {localStorage.getItem("level3Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton
					style={{
						color:'white',
						fontWeight: 'bold',
						backgroundColor: '#800000',
						}}
					>
                        LEVEL 4
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage4CustomerWallet}
					pageSize={this.state.stage4CustomerWallet.length}
					showPagination={false}
					
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}
		{localStorage.getItem("level4Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton
					style={{
						color:'white',
						fontWeight: 'bold',
						backgroundColor: '#FFA500',
						}}
					>
                        LEVEL 5
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage5CustomerWallet}
					pageSize={this.state.stage5CustomerWallet.length}
					showPagination={false}
					
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}		 
		{localStorage.getItem("level5Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton
					style={{
						color:'white',
						fontWeight: 'bold',
						backgroundColor: '#964B00',
						}}
					>
                        LEVEL 6
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage6CustomerWallet}
					pageSize={this.state.stage6CustomerWallet.length}
					showPagination={false}
					
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}	
				 
		{localStorage.getItem("level6Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        STAGE 7
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage7CustomerWallet}
					pageSize={this.state.stage7CustomerWallet.length}
					showPagination={false}
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}	
{localStorage.getItem("level7Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        STAGE 8
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage8CustomerWallet}
					pageSize={this.state.stage8CustomerWallet.length}
					showPagination={false}
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}				 
				 
				 {localStorage.getItem("level8Completed") === 'true' ?
          
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        STAGE 9
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <ReactTable
                    columns={columns}
                    data={this.state.stage9CustomerWallet}
					pageSize={this.state.stage9CustomerWallet.length}
					showPagination={false}
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				 : null}
        </Accordion>
            </div>

        )

    }
}

export default Wallet;


