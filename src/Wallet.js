import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';

class Wallet extends Component {

    constructor(props) {
        super(props)
        this.state = {
            availableBalance: '',
            customerWallet: []
        }

    }

    componentDidMount() {
        var schemeID = localStorage.getItem('schemeId');
        const url = myConstClass.WALLET_DETAILS + schemeID;
        console.log("url : " + url);
        Axios.get(url)
            .then((response) => {
                console.log("response : " + response);
                console.log("response data length : " + response.data);

                this.setState.availableBalance = response.data.availableWalletBalance;

                console.log("availableBalance data : " + response.data.availableWalletBalance);
                console.log("response data customerWallet  : " + response.data.schemeRefferalDetails);
                if (response.data.schemeRefferalDetails != null) {

                    this.setState({
                        availableBalance: response.data.availableWalletBalance,
                        customerWallet: response.data.schemeRefferalDetails
                    })
                    console.log("customerWallet : " + this.state.customerWallet);
                }
            })

            .catch(error => {
                console.log(error);
            });
    }


    render() {
        const columns = [
            {
                Header: "Referral ID",
                accessor: "referralId"
            },
            {
                Header: "Referral Name",
                accessor: "refferalName"
            },
            {
                Header: "Referral Date",
                accessor: "referralDate"
            }
            ,
            {
                Header: "Referral Level",
                accessor: "referralLevel"
            }
        ]
        return (
            <div id="wallet">
                <h1>Wallet Details</h1>
				<h3>Scheme ID : {localStorage.getItem('schemeId')}</h3>
				
                <h3>Available Balance : {this.state.availableBalance}</h3>


                <ReactTable
                    columns={columns}
                    data={this.state.customerWallet}
                >

                </ReactTable>
            </div>

        )

    }
}

export default Wallet;


