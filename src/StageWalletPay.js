import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-accessible-accordion/dist/fancy-example.css';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Slide from '@material-ui/core/Slide';
import SweetAlert from 'react-bootstrap-sweetalert';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
class StageWalletPay extends Component {

    constructor(props) {
        super(props)
        this.state = {
            availableBalance: '',
            isDialogOpen: false,
            schemeId:'',
            amount:'',
            alert:null,
			startDate:'',
			toDate:'',
			level:'',
			stage2CustomerWallet: [],
			stage3CustomerWallet: [],
			stage4CustomerWallet: [],
			stage5CustomerWallet: [],
			stage6CustomerWallet: [],
			stage7CustomerWallet: [],
			stage8CustomerWallet: [],
			stage9CustomerWallet: [],
			stage1CustomerWallet:[]
        
		}
    }
    handlePay = (e) => {
		
		console.log(this.state.schemeId);
		
		console.log(this.state.level);
		
		
		
		 const urlPay = myConstClass.STAGE_WALLET_PAY;
		const paramsPay =
		   {
        
          schemeId: this.state.schemeId,
		   stageLevel: this.state.level
		  }
		
		Axios.post(urlPay, paramsPay).then(response => {       
      
		}).catch(error => {
        console.log(error.message);
		});
		
        const getAlert = () => (
            <SweetAlert
              success
              title={"Payment updated successfully!"}
              onConfirm={() => this.hideAlert()}
              value={"Payment Updated"}
            />
            
          );
		  
		  console.log('level : '+this.state.level);
		  
		  
	 
		
		if('level1' === this.state.level){
			const rows = this.state.stage1CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage1CustomerWallet: rows
        })
		
		  } else if('level2' === this.state.level){
			  
			  const rows = this.state.stage2CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage2CustomerWallet: rows
        })
			
		  } else if('level3' === this.state.level){
			   
			 const rows = this.state.stage3CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage3CustomerWallet: rows
        })
		
		  } else if('level4' === this.state.level){
			  
			  const rows = this.state.stage4CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage4CustomerWallet: rows
        })
			   
		  } else if('level5' === this.state.level){
			  const rows = this.state.stage5CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage5CustomerWallet: rows
        })
			
		  } else if('level6' === this.state.level){
			  const rows = this.state.stage6CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage6CustomerWallet: rows
        })
			 
		  } else if('level7' === this.state.level){
			  
			  const rows = this.state.stage7CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage7CustomerWallet: rows
        })
		  } else if('level8' === this.state.level){
			  const rows = this.state.stage8CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage8CustomerWallet: rows
        })
			
		  } else if('level9' === this.state.level){
			 
			 const rows = this.state.stage9CustomerWallet.filter((row) => 
		  
		  row.schemeId !== this.state.schemeId
		  
		  );
		  
		  this.setState({
          stage9CustomerWallet: rows
        })
		  } 
		  
		
		
		
		
		
		
		
		  
		  
       this.setState(
	   {alert:getAlert(),
    isDialogOpen:false});
      }
	  
	  
	  dateFilterLevel1 = (e) => {
		  console.log( 'dateFilterSubmit1');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }
	 
 dateFilterLevel2 = (e) => {
		  console.log( 'dateFilterSubmit2');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level2'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage2CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel3 = (e) => {
		  console.log( 'dateFilterSubmit3');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level3'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage3CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel4 = (e) => {
		  console.log( 'dateFilterSubmit4');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level4'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage4CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel5 = (e) => {
		  console.log( 'dateFilterSubmit5');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level5'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage5CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel6 = (e) => {
		  console.log( 'dateFilterSubmit6');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level6'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage6CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel7 = (e) => {
		  console.log( 'dateFilterSubmit7');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level7'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage7CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel8 = (e) => {
		  console.log( 'dateFilterSubmit8');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level8'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage8CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }

 dateFilterLevel9 = (e) => {
		  console.log( 'dateFilterSubmit9');
		   const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
        
          fromDate: this.state.startDate,
          toDate: this.state.toDate,
		   stageLevel: 'level9'
		  }
		
		Axios.post(url, params).then(response => {       
        this.setState({
          stage9CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
	  }	  
	  
	  
	 	  
	  stage1Click = (e) => {
		console.log( "Stage 1 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }
	  
	  
	  
	  stage2Click = (e) => {
		console.log( "Stage 2 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level2'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage2CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage3Click = (e) => {
		console.log( "Stage 3 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level3'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage3CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
		
      }

	  stage4Click = (e) => {
		console.log( "Stage 4 clicked");
		         this.setState({
		  startDate:'',
			toDate:''
        })
		
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level4'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
         this.setState({
          stage4CustomerWallet: response.data,
		  startDate:'',
			toDate:''
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage5Click = (e) => {
		console.log( "Stage 5 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level5'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage5CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
		
      }

	  stage6Click = (e) => {
		console.log( "Stage 6 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level6'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage6CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage7Click = (e) => {
		console.log( "Stage 7 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level7'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage7CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage8Click = (e) => {
		console.log( "Stage 8 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level8'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage8CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }

	  stage9Click = (e) => {
		console.log( "Stage 9 clicked");
		   this.setState({
		  startDate:'',
			toDate:''
        })
		const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
		const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level9'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage9CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
      }	  
	  
    hideAlert=() =>{
        this.setState({alert:null});
    }
    handleClose = () => {
        this.setState({
            isDialogOpen:false
          })
      };
      handleOpen = ( schemeId ,  amount, e) => {
          		
				
		if('100' === amount){
			
			    var i = this.state.stage1CustomerWallet.find(item=>{
						return item.schemeId=== schemeId
				})
				var level = 'level1';
		  } else if('300' === amount){
			    var i = this.state.stage2CustomerWallet.find(item=>{
						return item.schemeId=== schemeId
				})
				var level = 'level2';
		  } else if('4700' === amount){
			    var i = this.state.stage3CustomerWallet.find(item=>{
						return item.schemeId=== schemeId
				})
				var level = 'level3';
		  } else if('20000' === amount){
			    var i = this.state.stage4CustomerWallet.find(item=>{
						return item.schemeId=== schemeId
				})
				var level = 'level4';
		  } else if('490000' === amount){
			    var i = this.state.stage5CustomerWallet.find(item=>{
						return item.schemeId=== schemeId
				})
				var level = 'level5';
		  } else if('9600000' === amount){
			    var i = this.state.stage6CustomerWallet.find(item=>{
						return item.schemeId=== schemeId
				})
				var level = 'level6';
		  } 
		  
        
        if(i){
        this.setState({
            isDialogOpen:true,
            schemeId:i.schemeId,
            amount:i.stageAmount,
			level: level
          })
        } 
      };
        
    componentDidMount() {
		
		console.log('componentDidMount');
      
        const url = myConstClass.STAGE_WALLET_PAY_DETAILS;
	const params =
		   {
          fromDate: '',
          toDate: '',
		  stageLevel: 'level1'
		  }
		
		Axios.post(url, params).then(response => {
        console.log("response " + response);
        console.log("response data : " + response.data);
       
        this.setState({
          stage1CustomerWallet: response.data
        })
		}).catch(error => {
        console.log(error.message);
		});
		
         
		
    }
   
    rowWalletPay(original){
        //return(<span></span>);
        return(
		(original.stageWalletStatus) ?(<span>{original.stageWalletStatus}</span>):(<button style={{  borderRadius: "5px", padding: "5px",  }}   onClick={this.handleOpen.bind(this,original.schemeId , original.stageAmount)} >Pay</button>));
    };


    render() {
        const columns = [
            {
                Header: "Scheme ID",
                accessor: "schemeId"
            },
			{
                Header: "First Name",
                accessor: "schemeFirstName"
            },
            {
                Header: "Completed Date",
                accessor: "stageCompletedDate"
            },
            {
                Header: "Amount",
                accessor: "stageAmount"
            }
            ,
			 {
                Header: "Wallet Status",
                accessor: "stageWalletStatus",
                Cell:({row, original})  =>  this.rowWalletPay( original),//{props.value==="paid"?(<span value={props.value}>{props.value}</span>):(<button value={props.value}>Pay</button>)},
            }
        ]
	
	  const {alert}=this.state;
        return (
            <div id="wallet">
                <h1>Scheme Wallet Pay </h1>
				
				
				<Accordion allowZeroExpanded preExpanded={['expire']}>
            <AccordionItem uuid="expire">
                <AccordionItemHeading onClick={this.stage1Click}>
                    <AccordionItemButton>
                       STAGE 1
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
				
		    <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel1} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage1CustomerWallet}
					defaultPageSize={10}
					filterable
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
			

            <AccordionItem>
                <AccordionItemHeading onClick={this.stage2Click}>
                    <AccordionItemButton  >
                        STAGE 2
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel2} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage2CustomerWallet}
					defaultPageSize={10}
					filterable
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage3Click}>
                    <AccordionItemButton>
                        STAGE 3
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel3} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage3CustomerWallet}
					defaultPageSize={10}
					filterable
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage4Click}>
                    <AccordionItemButton>
                        STAGE 4
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel4} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage4CustomerWallet}
					defaultPageSize={10}
					filterable
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage5Click}>
                    <AccordionItemButton>
                        STAGE 5
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                   <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel5} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage5CustomerWallet}
					defaultPageSize={10}
					filterable
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            <AccordionItem>
                <AccordionItemHeading onClick={this.stage6Click}>
                    <AccordionItemButton>
                        STAGE 6
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                  <div style={{ backgroundColor: 'powderblue', padding:"12px" }}>
          <span style={{ fontSize: "18px", fontWeight: "500" }}>From Date : </span>
          <DatePicker
            selected={this.state.startDate}
            onChange={date => this.setState({startDate:date})}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span style={{ fontSize: "18px", fontWeight: "500" }}> To Date : </span>
          <DatePicker
            selected={this.state.toDate}
            onChange={date => this.setState({toDate:date})}
            minDate={this.state.startDate}
            maxDate={new Date()}           
            dateFormat="dd/MM/yyyy"
          />
          <span>   </span>
          <button style={{ fontSize: "18px", fontWeight: "500", borderRadius: "5px", padding: "1px", backgroundColor: "THISTLE", }} onClick={this.dateFilterLevel6} >&nbsp; &nbsp;Submit &nbsp; &nbsp; </button></div>
    
        <ReactTable
                    columns={columns}
                    data={this.state.stage6CustomerWallet}
					defaultPageSize={10}
					filterable
                >

                </ReactTable>
                </AccordionItemPanel>
            </AccordionItem>
				
            
				
        </Accordion>
		
		
        <Dialog
        open={this.state.isDialogOpen}
        //TransitionComponent={Transition}
        keepMounted
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{"Wallet Pay"}</DialogTitle>
        <DialogContent>
        <div align="left" style={{marginLeft:"",display: 'flex', flexWrap:"wrap"}}>       
        <TextField
          id="standard-number"
          label="Scheme ID"
          value={this.state.schemeId || ''}  
          InputProps={{readOnly: true,}}  
          style={{width: '25ch', margin:"1em"}}                
        /> 
         <TextField
          id="standard-number"
          label="Amount"
          value={this.state.amount || ''}  
          InputProps={{readOnly: true,}}  
          style={{width: '25ch', margin:"1em"}}                   
        /> 
        </div >
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handlePay} color="primary">
            Pay
          </Button>
          <Button onClick={this.handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      {this.state.alert}
            </div>

        )

    }
}

export default StageWalletPay;


