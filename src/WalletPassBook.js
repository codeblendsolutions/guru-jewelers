import React, { Component } from 'react';
import Axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import * as myConstClass from './Constants.js';

class WalletPassBook extends Component {

    constructor(props) {
        super(props)
        this.state = {
            // availableBalance: '',
            walletPassBook: []
        }

    }


    componentDidMount() {


        var schemeID = localStorage.getItem('schemeId');

        const url = myConstClass.WALLET_PASS_BOOK + schemeID;

        console.log("url : " + url);
        Axios.get(url)
            .then((response) => {
                console.log("respoinse : " + response);
                console.log("response data : " + response.data);
                console.log("response dnullata : " + response.data === null);
                console.log("response length : " + response.data.length);
                if (response.data.length > 0) {
                    this.setState({
                        walletPassBook: response.data
                    })
                }
                console.log("walletPassBook : " + this.state.walletPassBook);

            })

            .catch(error => {
                console.log(error);
            });
    }


    render() {
        const columns = [
            {
                Header: "Date",
                accessor: "date"
            },
            {
                Header: "Particulars",
                accessor: "comments"
            },
            {
                Header: "Credit/Debit",
                accessor: "status"
            },
            {
                Header: "Amount",
                accessor: "amount"
            },
            {
                Header: "Available Balance",
                accessor: "availableBalance"
            }
        ];

       


        return (
            <div id="wallet">
                <h1>Wallet Paid History</h1>
                
                    <ReactTable

                        columns={columns}
                        data={this.state.walletPassBook}
                    >
                    </ReactTable>
                
                
            </div>

        )

    }
}

export default WalletPassBook;


